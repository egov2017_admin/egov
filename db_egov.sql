#@current version 0.9
#@date 9.13
#@最后一次修改人 dsf
#@本次修改内容：@DSF把部门导航和站点导航分开
#            @JXD去除对部门表的约定性限制
#            @ALL把时间统一用为DATETIME，不为空的DEFAULT now()
# 新增de_egov_readme.md，可以查看更多数据库相关信息
# 删除数据库
DROP DATABASE egov;
# 创建数据库
CREATE DATABASE egov;
#适用数据库
USE egov;
SET FOREIGN_KEY_CHECKS = 0;

#①公告公示表（政府令，关于xx的通知之类）
#参考链接http://public.zhengzhou.gov.cn/info/index.jhtml?a=theme&n=0
#id
#公告标题
#文号（如：政府令第200号）
#体裁（命令默认0 http://public.zhengzhou.gov.cn/02Z/61755.jhtml，通知默认1 http://public.zhengzhou.gov.cn/08EB/490478.jhtml）
#发布部门（一般是郑州市人民政府，仅做名称记录）
#发布日期
#成文日期
#生效日期
#废除日期
#公告内容（附件.pdf形式提供下载,数据库中存文件路径）
CREATE TABLE `egov_announcement` (
  `annId`             INT(6) UNSIGNED NOT NULL  AUTO_INCREMENT,
  `annNum`            VARCHAR(20)     NOT NULL,
  `anntype`           TINYINT(1)                DEFAULT 0,
  `annTitle`          VARCHAR(100)    NOT NULL,
  `annPostDepartment` VARCHAR(40)     NOT NULL,
  `annPostDate`       DATETIME        NOT NULL  DEFAULT now(),
  `annMakerData`      DATETIME        NOT NULL  DEFAULT now(),
  `annCarryData`      DATETIME        NOT NULL  DEFAULT now(),
  `annAbolishData`    DATETIME        NOT NULL,
  `annContent`        TEXT            NOT NULL,
  PRIMARY KEY (`annId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#②政策法规表
#参考链接http://www.zzcredit.gov.cn/gjzc/
#id
#法规标题
#来源(可以是部门或机构、组织，如市信用中心http://www.zzcredit.gov.cn/zzzc/20170902/2049.html)
#发布日期
#法规内容
CREATE TABLE `egov_law` (
  `lawId`       INT(6) UNSIGNED NOT NULL  AUTO_INCREMENT,
  `lawTitle`    VARCHAR(100)    NOT NULL,
  `lawSource`   VARCHAR(40)     NOT NULL,
  `lawPostDate` DATETIME        NOT NULL  DEFAULT now(),
  `lawContent`  TEXT            NOT NULL,
  PRIMARY KEY (`lawId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#③新闻中心表
#参考链接 http://www.zhengzhou.gov.cn/html/www/news1/20170906/490505.html
#id
#新闻标题
#新闻来源
#发布日期
#新闻内容
#引用图片
CREATE TABLE `egov_news` (
  `newId`       INT(6) UNSIGNED NOT NULL  AUTO_INCREMENT,
  `newTitle`    VARCHAR(100)    NOT NULL,
  `newSource`   VARCHAR(40)     NOT NULL,
  `newPostDate` DATETIME        NOT NULL  DEFAULT now(),
  `newContent`  TEXT            NOT NULL,
  `newPicture`  VARCHAR(200)    NOT NULL,
  PRIMARY KEY (`newId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#④机构部门表（要求每个部门作出详情页）
#参考链接 http://www.zhengzhou.gov.cn/html/www/department/
#参考链接 http://rexian.beijing.gov.cn/default/com.web.write.zmhdWriteLetter.flow?type=1   【选择部门】
#数据#参考链接【http://www.zzdrc.gov.cn/bmzz.jhtml】
#id
#部门名称
#部门英文名称
#部门类型（0 市政府部门；1 行业或公共企事业部门；2派出机构；3特殊，为导航表使用【考虑一下这种表设计方式是否合理】）
#部分负责人
#部门职能
#通讯地址
#邮政编码
#联系电话
#官方主页
CREATE TABLE `egov_department` (
  `depId`          INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `depName`        VARCHAR(60)     NOT NULL,
  `depEnglishName` VARCHAR(100)    NOT NULL,
  `depType`        TINYINT(1)      NOT NULL,
  `depManager`     VARCHAR(20)     NOT NULL,
  `depFunction`    TEXT            NOT NULL,
  `depAddress`     VARCHAR(60)     NOT NULL,
  `depZipCode`     INT(6)          NOT NULL DEFAULT 450000,
  `depPhone`       VARCHAR(14)     NOT NULL,
  `depIndexLink`   VARCHAR(50),
  PRIMARY KEY (`depId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#⑤办事规程表
#参考链接http://public.zhengzhou.gov.cn/guide/388792.jhtml
#参考链接http://public.zhengzhou.gov.cn/guide/index.jhtml?c=2&t=1
#字段如下：
#id
#办理主体
#服务对象service Object【缩写为SO】
#设定依据
#申请条件application requirements【缩写为AR】
#办理材料
#办理流程
#办理时限
#并联审批parallel examine and approve【缩写为PEAA，0表示否，1表示是】
#需现场勘查field investigation【0表示否，1表示是】
#最终决定机关Final decision organization【缩写为FDO】(允许为空)
#收费【0表示否，1表示是】
#收费标准scale of fees【缩写为SF】
#最终结果文件Final result file【缩写为FRF】（允许为空）
#办理时间Service time【缩写为ST】
#办理地点Service address【缩写为sa】
#联系电话connect phonennumber【缩写为CPN】
#投诉电话support hotline【缩写为sh】
#相关表格Related forms【缩写为RF】(允许为空)
#常见问题frequently asked questions【缩写为FAQ】（允许为空）
#备注
#相关信息related information【缩写为RI】（允许为空）
#相关法规Relevant laws and regulations【缩写为RLAR】(允许为空)
CREATE TABLE `egov_service_guide` (
  `sgId`        INT(6)          NOT NULL  AUTO_INCREMENT,
  `sgTitle`     VARCHAR(20)     NOT NULL,
  `depId`       INT(6) UNSIGNED NOT NULL,
  `sgso`        VARCHAR(10)     NOT NULL,
  `sgevidence`  TEXT            NOT NULL,
  `sgar`        VARCHAR(400)    NOT NULL,
  `sgdoc`       VARCHAR(200)    NOT NULL,
  `sgprogress`  VARCHAR(20000)  NOT NULL,
  `sgtimeLimit` VARCHAR(10)     NOT NULL,
  `sgpeaa`      TINYINT(1)      NOT NULL  DEFAULT 0,
  `sgfdo`       TINYINT(1)                DEFAULT 0,
  `sgchange`    TINYINT(1)      NOT NULL  DEFAULT 0,
  `sgsf`        VARCHAR(50)     NOT NULL,
  `sgst`        VARCHAR(50)     NOT NULL,
  `sgsa`        VARCHAR(50)     NOT NULL,
  `sgcpn`       VARCHAR(14)     NOT NULL,
  `sgrf`        VARCHAR(50),
  `sgfaq`       VARCHAR(200),
  `sgremark`    VARCHAR(200),
  `sgri`        VARCHAR(200),
  `sgRLAR`      VARCHAR(200),
  PRIMARY KEY (`sgId`),
  FOREIGN KEY (`depID`) REFERENCES `egov_department` (`depID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
#⑥导航服务表
CREATE TABLE `egov_navigation` (
  `navId`       INT(6) UNSIGNED NOT NULL          AUTO_INCREMENT PRIMARY KEY,
  `navName`     VARCHAR(20)     NOT NULL,
  `navLink`     VARCHAR(50)     NOT NULL,
  `navType`     TINYINT(1)      NOT NULL          DEFAULT 0,
  `navPostDate` DATETIME        NOT NULL          DEFAULT now()
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#⑦操作指南表
#参考链接http://public.zhengzhou.gov.cn/news/471206.jhtml
#id
#操作指南标题
#针对人群(默认0表示普通用户，1表示公务员)
#发布日期
CREATE TABLE `egov_operate_guide` (
  `OGid`    INT(6) UNSIGNED NOT NULL  AUTO_INCREMENT,
  `OGtitle` VARCHAR(30),
  `OGaim`   TINYINT(1)      NOT NULL  DEFAULT 0,
  `OGdate`  DATETIME        NOT NULL  DEFAULT now(),
  PRIMARY KEY (`OGid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#⑧网上咨询反馈表（和市长信箱相比不允许公开个人信息）
#参考链接http://rexian.beijing.gov.cn/default/com.web.write.zmhdWriteLetter.flow?type=1 http://222.143.36.37/mbzzic/messageAction?requestId=showMessagePage
#id
#主题
#反馈部门
#反馈类型（0咨询；1建议；2表扬；3投诉；4举报；5反弹；）
#是否公开
#姓名
#电话
#居住地址
#个人邮箱
#内容
#相关材料上传
#提交日期
#解决日期（即为回复日期）
#解决状态（0 未解决；1已解决 ）
#回复内容
#唯一查询码
CREATE TABLE `egov_consulting_and_feedback` (
  `cafId`           INT(6) UNSIGNED NOT NULL  AUTO_INCREMENT,
  `cafTitle`        VARCHAR(40)     NOT NULL,
  `depId`           INT(6) UNSIGNED NOT NULL,
  `cafType`         TINYINT(1)      NOT NULL,
  `cafPublic`       TINYINT(1)      NOT NULL  DEFAULT 1,
  `name`            VARCHAR(10)     NOT NULL,
  `phone`           VARCHAR(14)     NOT NULL,
  `address`         VARCHAR(100)    NOT NULL,
  `email`           VARCHAR(100),
  `cafContent`      TEXT            NOT NULL,
  `cafFile`         VARCHAR(200),
  `cafPostDate`     DATETIME        NOT NULL  DEFAULT now(),
  `cafReplyDate`    DATETIME,
  `cafSolvedStatus` TINYINT(1)      NOT NULL  DEFAULT 0,
  `cafReply`        TEXT,
  `cafSearchKey`    VARCHAR(32)     NOT NULL,
  PRIMARY KEY (`cafId`),
  FOREIGN KEY (`depId`) REFERENCES `egov_department` (`depId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
#⑨政府信箱表(市长信箱)
#参考链接 http://szxx.beijing.gov.cn/main.jsp
#id
#类型0建议1投诉2举报3指南4其他
#主题
#内容
#姓名
#电话
#居住地址
#个人邮箱
#相关材料上传
#是否公开
#是否保护隐私
#邮件发送时间
#回复内容
#回复日期
#唯一查询码
CREATE TABLE `egov_mailbox` (
  `mailId`    INT(10) UNSIGNED NOT NULL  AUTO_INCREMENT,
  `mailType`  TINYINT(1)       NOT NULL  DEFAULT 0,
  `mailTitle` VARCHAR(50)      NOT NULL,
  `name`      VARCHAR(10)      NOT NULL,
  `phone`     VARCHAR(14)      NOT NULL,
  `address`   VARCHAR(100),
  `email`     VARCHAR(100),
  `ispublic`  TINYINT                    DEFAULT 1,
  `content`   TEXT             NOT NULL,
  `postdate`  DATETIME         NOT NULL  DEFAULT now(),
  `reply`     TEXT,
  `replydate` DATETIME,
  `searchKey` VARCHAR(17)      NOT NULL,
  PRIMARY KEY (`mailId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#⑩网上调查
#包含⑩-1征集意见表和⑩-2意见回复表

#⑩-1征集意见表
#id
#意见标题
#描述信息
#发布部门
#发布日期
#截止日期
CREATE TABLE `egov_required_advice` (
  `id`          INT UNSIGNED    NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `title`       VARCHAR(200)    NOT NULL,
  `description` TEXT            NOT NULL,
  `depid`         INT(6) UNSIGNED NOT NULL,
  `Postdate`    DATETIME        NOT NULL DEFAULT now(),
  `deadline`    DATETIME        NOT NULL,
  FOREIGN KEY (`depid`) REFERENCES `egov_department` (`depId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#⑩-2意见回复表
#id
#回复主题
#内容
#名字
#电话
#电子邮件
CREATE TABLE `egov_advice` (
  `id`      INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `replySubject` INT UNSIGNED NOT NULL,
  `content` TEXT         NOT NULL,
  `name`       VARCHAR(32)  NOT NULL,
  `phone`      VARCHAR(14)  NOT NULL,
  `email`      VARCHAR(60),
  FOREIGN KEY (`replySubject`) REFERENCES `egov_required_advice` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#⑪论坛交流
#包含⑪-1帖子表和⑪-2回帖表
#⑪-1帖子表
#id
#帖子标题
#发帖人email
#帖子内容
#帖子图片
CREATE TABLE `egov_posts` (
  `posid`      INT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `postitle`   VARCHAR(60)   NOT NULL,
  `poscontent` VARCHAR(1000) NOT NULL,
  `pospicture` VARCHAR(300)           DEFAULT NULL,
  `email`      VARCHAR(60)   NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#⑪-2回帖表
#id
#回帖主题
#回帖人email
#发帖人email
CREATE TABLE `egov_posts_reply` (
  `pfid`       INT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `pfSubject`  INT UNSIGNED  NOT NULL,
  `pfreceiver` VARCHAR(60)   NOT NULL,
  `pfcontent`  VARCHAR(1000) NOT NULL,
  `pfpicture`  VARCHAR(300)           DEFAULT NULL,
  `email`      VARCHAR(60)   NOT NULL,
  FOREIGN KEY (`pfSubject`) REFERENCES `egov_posts` (`posid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#⑫政府网站找错表
#参考链接 http://121.43.68.40/exposure/jiucuo.html?site_code=4101000002&url=http%3A%2F%2Fwww.zhengzhou.gov.cn%2F
#id
#问题网站页面名称
#问题页面错误类型
#--0内容无法访问
#--1信息不更新
#--2内容不准确
#--3咨询留言不回复
#--4错别字
#--5虚假伪造内容
#--6其他
#问题页面链接
#截图上传
#问题描述
#纠错人姓名
#纠错人电话
#纠错人email
#曝光日期
#解决状态（0：未解决，1：已解决）
#解决日期
#回复内容
#唯一查询码
CREATE TABLE `egov_error_correction` (
  `erId`           INT UNSIGNED NOT NULL  AUTO_INCREMENT,
  `ersiteName`     VARCHAR(20)  NOT NULL,
  `erType`         TINYINT(1)   NOT NULL,
  `erLink`         VARCHAR(50)  NOT NULL,
  `erSCR`          VARCHAR(300) NOT NULL,
  `erContent`      TEXT         NOT NULL,
  `name`           VARCHAR(20)  NOT NULL,
  `phone`          VARCHAR(14)  NOT NULL,
  `email`          VARCHAR(60),
  `erDate`         DATETIME     NOT NULL  DEFAULT now(),
  `erSolvedStatus` TINYINT(1)   NOT NULL  DEFAULT 0,
  `erSolvedDate`   DATETIME,
  `erReply`        TEXT,
  `erSearchKey`    VARCHAR(15),
  PRIMARY KEY (`erId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#⑬管理员表
#id
#用户名
#密码
#类型
#0超级管理员（可以且只能对此表进行证删改查）
#1
CREATE TABLE `egov_admin` (
  `admId`       INT(3) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `admUserName` VARCHAR(10)     NOT NULL UNIQUE,
  `admPassword` VARCHAR(20)     NOT NULL,
  `admType`     TINYINT(1)      NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;