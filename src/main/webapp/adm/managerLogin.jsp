<%@ page import="java.net.URLDecoder" %><%--
  Created by IntelliJ IDEA.
  User: liu
  Date: 2017/9/22
  Time: 0:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String msg= (String) request.getAttribute("msg");
    if(msg==null||msg.length()==0){
        msg="";
    }
    request.setCharacterEncoding("utf-8");
    String username="";
    String type= "";
    Cookie[] cookies = request.getCookies();
    if(cookies!=null&&cookies.length>0)
    {
        for(Cookie c:cookies)
        {

            if(c.getName().equals("username"))
            {
                System.out.println(c);
                if(c.getMaxAge()!=0)
                username = URLDecoder.decode(c.getValue(),"utf-8");
            }
            if(c.getName().equals("type"))
            {
                System.out.println(c);
                if(c.getMaxAge()!=0)
                type = URLDecoder.decode(c.getValue(),"utf-8");
            }
        }
    }
    if(username.trim().length()>0&&type.trim().length()>0) {

        request.getSession().setAttribute("username",username);
        request.getSession().setAttribute("type",type);
        if(type.equals("0")) {
            response.sendRedirect("/adm/superManager.jsp");
        }else if(type.equals("1")){
            response.sendRedirect("/adm/mailBoxManager.jsp");
        }else if(type.equals("2")){
            response.sendRedirect("/adm/cafManager.jsp");
        }

    }
%>
<!DOCTYPE html>
<!--[if IE 8]>
<html xmlns="http://www.w3.org/1999/xhtml" class="ie8" lang="zh-CN">
<![endif]-->
<!--[if !(IE 8) ]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>egov网站后台管理 &lsaquo; 登录</title>
    <link rel='stylesheet' href='https://www.deepin.org/wp-admin/load-styles.php?c=1&amp;dir=ltr&amp;load%5B%5D=dashicons,buttons,forms,l10n,login&amp;ver=4.5.10' type='text/css' media='all' />
    <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.5.10' type='text/css' media='all' />
    <meta name='robots' content='noindex,follow' />
</head>
<body class="login login-action-login wp-core-ui  locale-zh-cn">
<div id="login">
    <h1><a href="#" title="网站后台管理" tabindex="-1" style="background-image: none,url(/images/logo.png);">egov网站后台管理</a></h1>

    <form name="loginform" id="loginform" action="/manager_login" method="post">
        <p>
            <label for="user_login">用户名<br />
                <input type="text" name="log" id="user_login" class="input" value="" size="20" /></label>
        </p>
        <p>
            <label for="user_pass">密码<br />
                <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" /></label>
        </p>
        <p class="forgetmenot"><label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever"  /> 记住我的登录信息</label></p>
        <p class="submit">
            <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="登录" />
            <input type="hidden" name="redirect_to" value="https://www.deepin.org/wp-admin/" />
            <input type="hidden" name="testcookie" value="1" />
        </p>
        <br>
        <br>
        <p style="color: red"><%=msg%></p>
    </form>


    <script type="text/javascript">
        function wp_attempt_focus(){
            setTimeout( function(){ try{
                d = document.getElementById('user_login');
                d.focus();
                d.select();
            } catch(e){}
            }, 200);
        }

        wp_attempt_focus();
        if(typeof wpOnload=='function')wpOnload();
    </script>

</div>


<div class="clear"></div>
</body>
</html>
