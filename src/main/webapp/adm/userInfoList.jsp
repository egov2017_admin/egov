<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    String search_name=(String) request.getAttribute("search_name");
    if(search_name==null) search_name="";
    Integer totalPages= (Integer) request.getAttribute("totalPages");
    String search_type=(String)request.getAttribute("search_type");
    String typeStr[]={"超级管理员","市长信箱管理员","咨询反馈管理员"};
    if(totalPages%5==0){
        totalPages/=5;
    }else {
        totalPages/=5;
        totalPages++;
    }
    ArrayList<Admin> userInfoList=(ArrayList<Admin>)request.getAttribute("userInfoList");

%>
<!DOCTYPE HTML>
<html>
<head>
    <title>后台管理-用户密码管理</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/styles/mailboxWrite.css">
</head>
<body>
<div class="main-wrap clearfix mt-30">
    <div class="content-box">
        <div class="box-title">后台管理-用户密码管理<span class="search-bar-icon"></span>
        </div>
        <form method="get" action="userInfoList" id="searchform" class="lm-search-bar">

            <input type="text" name="search_name" placeholder="用户名" value=<%=search_name%> >
            <select name="search_type">
                <option value="">请选择</option>
                <%
                    for(int i=0;i<typeStr.length;++i){
                        if(typeStr[i].equals(search_type)){
                %>
                <option value="<%=typeStr[i]%>" selected="selected"><%=typeStr[i]%></option>
                <%
                }else {
                %>
                <option value="<%=typeStr[i]%>" ><%=typeStr[i]%></option>
                <%
                        }
                    }
                %>
            </select>
            <input type="submit" value="查询">
        </form>
        <div class="lm-list">
            <%
                int len=userInfoList.size();
                for(int i=0;i<len;++i){
            %>
            <ul>
                <li>

                    <form  action="userInfoList" method="post">

                        <input type="hidden" name="id" value="<%=userInfoList.get(i).getAdmId()%>" />
                        用户名:
                        <input type="text" name="UserName" value="<%=userInfoList.get(i).getAdmUserName()%>"/>
                        密码:
                        <input type="password" name="Password" value=""/>
                        类型:
                        <select name="Type">
                            <option value="">请选择</option>
                            <%
                                for(int jj=0;jj<typeStr.length;++jj){
                                    if(typeStr[jj].equals(typeStr[userInfoList.get(i).getAdmType()])){
                            %>
                            <option value="<%=typeStr[jj]%>" selected="selected"><%=typeStr[jj]%></option>
                            <%
                            }else {
                            %>
                            <option value="<%=typeStr[jj]%>" ><%=typeStr[jj]%></option>
                            <%
                                    }
                                }
                            %>
                        </select>

                        <input type="submit"  class="button" value="提交修改">
                    </form>


                </li>
            </ul>
            <%
                }
            %>
            <div class="lm-list">

                <div class="page-tile">

                    <%
                        for(int i=0;i<totalPages;++i){
                    %>
                    <a href="/userInfoList?search_name=<%=search_name%>&search_type=<%=search_type%>&pageNum=<%=(i+1)%>"><%=(i+1)%>
                    </a>
                    <%
                        }
                    %>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="copyright">
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>