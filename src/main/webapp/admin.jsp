<%--
  Created by IntelliJ IDEA.
  User: dsf
  Date: 2017/9/21
  Time: 8:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>壹帽 &lsaquo; 登录</title>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel='stylesheet'
          href='https://www.onehat.cn/wp-admin/load-styles.php?c=0&amp;dir=ltr&amp;load%5B%5D=dashicons,buttons,forms,l10n,login&amp;ver=4.8.2'
          type='text/css' media='all'/>
    <meta name='robots' content='noindex,follow'/>
    <meta name="viewport" content="width=device-width"/>
    <link rel="icon" href="http://www.onehat.cn/wp-content/uploads/2016/09/cropped-Hat-32x32.png" sizes="32x32"/>
    <link rel="icon" href="http://www.onehat.cn/wp-content/uploads/2016/09/cropped-Hat-192x192.png" sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed"
          href="http://www.onehat.cn/wp-content/uploads/2016/09/cropped-Hat-180x180.png"/>
    <meta name="msapplication-TileImage"
          content="http://www.onehat.cn/wp-content/uploads/2016/09/cropped-Hat-270x270.png"/>
</head>
<body class="login login-action-login wp-core-ui  locale-zh-cn">
<div id="login">
    <form name="loginform" id="loginform" action="https://www.onehat.cn/wp-login.php" method="post">
        <p>
            <label for="user_login">用户名<br/>
                <input type="text" name="log" id="user_login" class="input" value="" size="20"/></label>
        </p>
        <p>
            <label for="user_pass">密码<br/>
                <input type="password" name="pwd" id="user_pass" class="input" value="" size="20"/></label>
        </p>
        <p class="forgetmenot"><label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme"
                                                              value="forever"/> 记住我的登录信息</label></p>
        <p class="submit">
            <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="登录"/>
            <input type="hidden" name="redirect_to" value="https://www.onehat.cn/wp-admin/"/>
            <input type="hidden" name="testcookie" value="1"/>
        </p>
    </form>

    <script type="text/javascript">
        function wp_attempt_focus() {
            setTimeout(function () {
                try {
                    d = document.getElementById('user_login');
                    d.focus();
                    d.select();
                } catch (e) {
                }
            }, 200);
        }

        /**
         * Filters whether to print the call to `wp_attempt_focus()` on the login screen.
         *
         * @since 4.8.0
         *
         * @param bool $print Whether to print the function call. Default true.
         */
        wp_attempt_focus();
        if (typeof wpOnload == 'function') wpOnload();
    </script>


</div>


<div class="clear"></div>
</body>
</html>
