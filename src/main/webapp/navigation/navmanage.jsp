<%@ page import="bean.Navigation" %><%--
  Created by IntelliJ IDEA.
  User: dsf
  Date: 2017/9/13
  Time: 19:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/normalize.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/grid.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">

<html>
<head>
    <title>导航条目管理</title>
</head>
<body>

<%--<form id="myform" name="myform" action="/navigation"--%>
<%--method="get">--%>
<%--</form>--%>

<div class="main">
    <div class="body container">
        <div class="typecho-page-title">
            <h2>导航条目管理<a href="https://www.onehat.cn/literature/admin/write-post.php">新增</a></h2>
        </div>
        <div class="row typecho-page-main" role="main">
            <div class="col-mb-12 typecho-list">
                <div class="typecho-list-operate clearfix">
                    <%--<form method="get">--%>
                        <div class="operate">
                            <label><i class="sr-only">全选</i><input type="checkbox"
                                                                   class="typecho-table-select-all"/></label>
                            <div class="btn-group btn-drop">
                                <button class="btn dropdown-toggle btn-s" type="button"><i class="sr-only">操作</i>选中项 <i
                                        class="i-caret-down"></i></button>
                                <ul class="dropdown-menu">
                                    <li><a lang="你确认要删除这些文章吗?"
                                           href="https://www.onehat.cn/literature/index.php/action/contents-post-edit?do=delete&_=5c03e5e048f4c316c41117fb5227ffc0">删除</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="search" role="search">
                            <input type="text" class="text-s" placeholder="请输入关键字" value="" name="keywords"/>
                            <%--<select name="category">--%>
                            <%--<option value="">所有分类</option>--%>
                            <%--<option value="1">默认分类</option>--%>
                            <%--</select>--%>
                            <button type="submit" class="btn btn-s">筛选</button>
                        </div>
                    <%--</form>--%>
                </div><!-- end .typecho-list-operate -->

                <form method="get" name="manage_get" action="/navigation">
                    <div class="typecho-table-wrap">
                        <table class="typecho-list-table">
                            <colgroup>
                                <col width="20"/>
                                <col width="6%"/>
                                <col width="45%"/>
                                <col width=""/>
                                <col width="18%"/>
                                <col width="16%"/>
                            </colgroup>
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>编号</th>
                                <th>名称</th>
                                <th>链接</th>
                                <th>日期</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr id="post-1">
                                <td><input type="checkbox" value="1" name="cid[]"/></td>
                                <td>site</td>
                                <td>
                                    <a href="https://www.onehat.cn/literature/admin/write-post.php?cid=1">${navId}
                                    </a>
                                    <a href="https://www.onehat.cn/literature/index.php/default/start.html"
                                       title="浏览 Hello World"><i class="i-exlink"></i></a>
                                </td>
                                <td>
                                    <%--<a href="https://www.onehat.cn/literature/admin/manage-posts.php?uid=1"><%=navSite.getNavName()%>--%>
                                    </a>
                                </td>
                                <td>
                                    <%--<a href="https://www.onehat.cn/literature/admin/manage-posts.php?category=1"><%=navSite.getNavLink()%>--%>
                                    </a>
                                </td>
                                <td>
                                    <%--<%=navSite.getNavPostDate()%>--%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form><!-- end .operate-form -->

                <div class="typecho-list-operate clearfix">
                    <%--<form method="post">--%>
                        <div class="operate">
                            <label><i class="sr-only">全选</i><input type="checkbox"
                                                                   class="typecho-table-select-all"/></label>
                            <div class="btn-group btn-drop">
                                <button class="btn dropdown-toggle btn-s" type="button"><i class="sr-only">操作</i>选中项 <i
                                        class="i-caret-down"></i></button>
                                <ul class="dropdown-menu">
                                    <li><a lang="你确认要删除这些文章吗?"
                                           href="https://www.onehat.cn/literature/index.php/action/contents-post-edit?do=delete&_=5c03e5e048f4c316c41117fb5227ffc0">删除</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <ul class="typecho-pager">
                            <li class="current"><a
                                    href="https://www.onehat.cn/literature/admin/manage-posts.php?page=1">1</a></li>
                        </ul>
                    <%--</form>--%>
                </div><!-- end .typecho-list-operate -->
            </div><!-- end .typecho-list -->
        </div><!-- end .typecho-page-main -->
    </div>
</div>


</body>
</html>
