<%--
  Created by IntelliJ IDEA.
  User: dsf
  Date: 2017/9/8
  Time: 19:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="keywords" content=""/>
    <meta http-equiv="description" content=""/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <title>交流互动 - 郑州市人民政府</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/ms_2_css.css"></LINK>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/ms_2_main.css"></LINK>
    <script src="/r/cms/jquery.js" type="text/javascript"></script>
    <script src="/r/cms/front.js" type="text/javascript"></script>
    <script src="/r/cms/analytics.js" type="text/javascript"></script>
</head>
<body>
<script src="/r/cms/www/cn/js/less.version.js" type="text/javascript"></script>
<script src="/r/cms/www/cn/js/ischrome.js" type="text/javascript"></script>
<div class="main-body">
    <div class="top-bar">
        <div class="bar">
            <div class="main-wrap clearfix">
                <div class="four-links">
                    <a href="http://www.gov.cn" target="_blank">中央人民政府</a>
                    <span class="link-sep"></span>
                    <a href="http://www.henan.gov.cn" target="_blank">河南省人民政府</a>
                    <span class="link-sep"></span>
                    <a href="http://www.zzrd.gov.cn" target="_blank">郑州市人大</a>
                    <span class="link-sep"></span>
                    <a href="http://www.zzzxy.gov.cn" target="_blank">郑州市政协</a>
                </div>
                <div class="version-links">
                    <a href="http://weibo.com/zzfb" target="_blank" title="新浪微博">
                        <img src="/r/cms/www/cn/images/weibo01.png" style="vertical-align:middle;">
                    </a>
                    <!--<a href="http://e.t.qq.com/zzszfwz" target="_blank" title="腾讯微博">
                        <img src="/r/cms/www/cn/images/weibo02.png" style="vertical-align:middle;">
                    </a>-->
                    &nbsp;
                    <a href="http://mail.zhengzhou.gov.cn" target="_blank">公务邮箱</a>
                    <span class="link-sep"></span>
                    <a id="accessible" href="javascript:;">无障碍阅读</a>
                    <span class="link-sep"></span>
                    <a id="chinese" data-default="简体" data-changed="繁體" href="javascript:;">繁體</a>
                </div>
            </div>
        </div>
    </div>
    <script src="/r/cms/www/cn/js/chinese.js" type="text/javascript"></script>
    <script src="/r/cms/www/cn/js/accessible.js" type="text/javascript"></script>
    <script type="text/javascript">
        //简繁体转换
        initChinese(document.querySelector("#chinese"), ["title"]);
        //无障碍
        initAccessible(document.querySelector(".main-body"), document.querySelector("#accessible"));
    </script>
    <div class="main-wrap main-container">
        <div class="main-nav">
            <a class="node" href="/">首页</a>
            <a class="node" href="http://www.zhengzhou.gov.cn/news1/index.jhtml">新闻中心</a>
            <a class="node" href="/html/www/view1.html">全景郑州</a>
            <a class="node" href="http://public.zhengzhou.gov.cn/info/index.jhtml" target="_blank">政府信息公开</a>
            <a class="node" href="http://www.zhengzhou.gov.cn/service/index.jhtml">公共服务</a>
            <a class="node focus" href="http://www.zhengzhou.gov.cn/ia/index.jhtml">交流互动</a>
            <a class="node" href="/html/www/department/">政府部门</a>
            <form action="/search.jspx" method="get" class="search-bar" onsubmit="return search_submit(this)">
                <input type="text" name="q">
                <input type="submit" value="">
                <em>查询间隔10秒</em>
            </form>
            <script type="text/javascript">
                var search_submit = function (form) {
                    var input = form.querySelector("input[name=q]");
                    var value = input.value.replace(/(^\s*)|(\s*$)/g, "");
                    if (value) {
                        //每10秒可以查询一次
                        if (jQuery.cookie("s")) {
                            var em = form.querySelector("em");
                            em.style.display = "block";
                            setTimeout(function () {
                                em.style.display = "none";
                            }, 2000);
                            return false;
                        } else {
                            input.value = value;
                            var date = new Date();
                            date.setTime(date.getTime() + 10000);
                            jQuery.cookie("s", value, {expires: date});
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            </script>
        </div>
        <div class="tile-region mt-region">
            <div class="tile-group gold">
                <a href="http://response.zhengzhou.gov.cn/mailbox/write.jspx?id=80" class="tile gold active color-green"
                   target="_blank">
                    <div class="wrap">
                        <div class="title-cn">给市长写信</div>
                        <div class="title-en">write to the mayor</div>
                        <div class="icon">E</div>
                    </div>
                </a>
                <a href="http://wsxfdt.hnxf.gov.cn:5557/sy/initPlatform.action" class="tile gold active color-sky-blue"
                   target="_blank">
                    <div class="wrap">
                        <div class="title-cn">阳光信访</div>
                        <div class="title-en">letters and calls</div>
                        <div class="icon">&</div>
                    </div>
                </a>
                <a href="http://12345.zhengzhou.gov.cn" class="tile gold active color-light-red" target="_blank">
                    <div class="wrap">
                        <div class="title-cn">市长电话</div>
                        <img class="title-icon" src="/r/cms/www/cn/images/tile/tile_icon_12345.png"/>
                        <div class="title-en">mayor hotlines</div>
                        <div class="icon">*</div>
                    </div>
                </a>
                <div class="tile gold">
                    <div class="wrap">
                        <img class="image" src="/r/cms/www/cn/images/decorations/deco_15.png"/>
                    </div>
                </div>
                <a href="http://222.143.36.37/mbzzic" class="tile gold active color-lime-green" target="_blank">
                    <div class="wrap">
                        <div class="title-cn">网络发言人</div>
                        <img class="title-icon" src="/r/cms/www/cn/images/tile/tile_icon_zzic.png"/>
                        <div class="title-en">internet conference</div>
                        <div class="icon">q</div>
                    </div>
                </a>
                <a href="http://xtq.zynews.com" class="tile gold active color-light-pink" target="_blank">
                    <div class="wrap">
                        <div class="title-cn"></div>
                        <img class="title-icon" src="/r/cms/www/cn/images/tile/tile_icon_xtq.png"/>
                        <div class="title-small">网络行政路 民意直通车</div>
                    </div>
                </a>
                <div class="tile gold color-slate-blue">
                    <div class="wrap">
                    </div>
                </div>
                <div class="tile gold color-yellow-green">
                    <div class="wrap">
                    </div>
                </div>
                <div class="tile gold full">
                    <div class="wrap">
                        <img class="image" src="/r/cms/www/cn/images/decorations/deco_16.png"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="main-wrap">
            <table cellpadding=0 cellspacing=0 border=0 width="100%" style="table-layout:fixed;">
                <tr>
                    <td align="right">
							<span class="biaoshi-wrap">
								<script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script>
								<script id="_jiucuo_" sitecode='4101000002'
                                        src='http://pucha.kaipuyun.cn/exposure/jiucuo.js'></script>
							</span>
                    </td>
                    <td width=580>
                        <a href="http://www.zhengzhou.gov.cn/szb06.jhtml" target="_blank">联系我们</a>
                        |
                        <a href="/html/www/declare.html" target="_blank">郑重声明</a>
                        |
                        <a href="/html/www/map.html" target="_blank">网站地图</a>
                        |
                        <a href="mailto:zhengzhougov@zhengzhou.gov.cn">意见反馈</a>
                        |
                        <a href="http://www.zhengzhou.gov.cn/cgi-bin/testgov" target="_blank">电子政务外网接入测试</a>
                        <br/>郑州市人民政府版权所有&nbsp;&nbsp;&nbsp;地址：郑州市中原中路233号&nbsp;&nbsp;&nbsp;邮编：450007
                        <br/>网站标识码：4101000002&nbsp;豫ICP备05012610号&nbsp;
                        <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=41010202002520"
                           target="_blank">
                            <img src="/r/cms/www/cn/images/gaba.png" style="vertical-align:top;">
                            豫公网安备 41010202002520号
                        </a>
                    </td>
                    <td align="left"><a href="http://www.zzwljc.com" target="_blank"><img
                            src="/r/cms/www/cn/images/110.png"/></a></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>