<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/caf.css">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
    <title>EGOV网络发言人</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/caf.js">
    </script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/form.validator.js">
    </script>
    <script src="${pageContext.request.contextPath}/js/jquery.js" type="text/javascript"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.mailautocomplete.js">
    </script>

</head>
<body style="background: url(${pageContext.request.contextPath}/images/bg.png)">
<div>
    <div class="main-body pubguide">

        <div class="main-wrap clearfix mt-30">
            <div class="content-box">
                <div class="box-title write-dp">写信说明<a class="h-button" href="/mailbox/search.jspx?id=80">查看以往信件</a>
                </div>
                <form id="form" name="form" method="post" calss="lm-form"
                      action="/caf" enctype="multipart/form-data">
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="clearfix">
                        <div class="write-input ">
                            <em>主题：<label style="color:red">*</label></em>
                            <span>
									<input type="text" name="title" vld="{required:true,isTitle:true}" value="${title}"
                                           id="caf_write_title"
                                           autocomplete="off" class=" long-input" value="" maxLength="50"/>
								</span>
                            <i></i>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="write-input left">
                            <em>办理单位：<label style="color:red">*</label></em>
                            <span>
                                <select class="MessageStyle" id="caf_write_depid" style="width:150px" name="depid">
                            <option value=""> 请选择办理单位</option>
                            <option value="金水区"> 金水区</option>
                            <option value="登封市"> 登封市</option>
                            <option value="荥阳市"> 荥阳市</option>
                            <option value="中原区"> 中原区</option>
                            <option value="二七区"> 二七区</option>
                            <option value="管城区"> 管城区</option>
                            <option value="惠济区 "> 惠济区</option>
                            <option value="上街区 "> 上街区</option>
                            <option value="中牟县"> 中牟县</option>
                            <option value="新郑市"> 新郑市</option>
                            <option value="新密市"> 新密市</option>
                            <option value="巩义市"> 巩义市</option>
                            <option value="郑州新区"> 郑州新区</option>
                            <option value="郑东新区"> 郑东新区</option>
                            <option value="高新技术开发区"> 高新技术开发区</option>
                            <option value="经济技术开发区"> 经济技术开发区</option>
                            <option value="郑州航空港区"> 郑州航空港区</option>
                            <option value="郑州出口加工区"> 郑州出口加工区</option>
                            <option value="郑州黄河生态旅游风景区管委会"> 郑州黄河生态旅游风景区管委会</option>
                            <option value="市工商行政管理局"> 市工商行政管理局</option>
                            <option value="市发展和改革委员会"> 市发展和改革委员会</option>
                            <option value="市教育局"> 市教育局</option>
                            <option value="市科学技术局"> 市科学技术局</option>
                            <option value="市工业和信息化委员会"> 市工业和信息化委员会</option>
                            <option value="市民族事务委员会"> 市民族事务委员会</option>
                            <option value="市国有资产监督管理委员会"> 市国有资产监督管理委员会</option>
                            <option value="市民族事务委员会"> 市民族事务委员会</option>
                            <option value="市公安局"> 市公安局</option>
                            <option value="市民政局"> 市民政局</option>
                            <option value="市园林局"> 市园林局</option>
                            <option value="市司法局"> 市司法局</option>
                            <option value="市畜牧局"> 市畜牧局</option>
                            <option value="市财政局"> 市财政局</option>
                            <option value="市文物局"> 市文物局</option>
                            <option value="市人力资源和社会保障局"> 市人力资源和社会保障局</option>
                            <option value="市人民防空办公室"> 市人民防空办公室</option>
                            <option value="市国土资源局"> 市国土资源局</option>
                            <option value="市政府法制办公室"> 市政府法制办公室</option>
                            <option value="市安全生产监督管理局"> 市安全生产监督管理局</option>
                            <option value="市物价局"> 市物价局</option>
                            <option value="市城乡建设委员会"> 市城乡建设委员会</option>
                            <option value="市政府外事侨务办公室"> 市政府外事侨务办公室</option>
                            <option value="市住房保障和房地产管理局"> 市住房保障和房地产管理局</option>
                            <option value="市信访局"> 市信访局</option>
                            <option value="市城乡规划局"> 市城乡规划局</option>
                            <option value="市粮食局"> 市粮食局</option>
                            <option value="市交通运输委员会"> 市交通运输委员会</option>
                            <option value="市旅游局"> 市旅游局</option>
                            <option value="市统计局"> 市统计局</option>
                            <option value="市城市管理局"> 市城市管理局</option>
                            <option value="市体育局"> 市体育局</option>
                            <option value="市审计局"> 市审计局</option>
                            <option value="市环境保护局"> 市环境保护局</option>
                            <option value="市人口和计划生育委员会"> 市人口和计划生育委员会</option>
                            <option value="市农业农村工作委员会"> 市农业农村工作委员会</option>
                            <option value="市水务局"> 市水务局</option>
                            <option value="市食品药品监督管理局"> 市食品药品监督管理局</option>
                            <option value="市林业局"> 市林业局</option>
                            <option value="市卫生局"> 市卫生局</option>
                            <option value="市商务局"> 市商务局</option>
                            <option value="市文化广电新闻出版局"> 市文化广电新闻出版局</option>
                            <option value="国家税务局"> 国家税务局</option>
                            <option value="地方税务局"> 地方税务局</option>
                            <option value="市气象局"> 市气象局</option>
                            <option value="市质量技术监督局"> 市质量技术监督局</option>
                            <option value="郑州市爱国卫生运动委员会办公室"> 郑州市爱国卫生运动委员会办公室</option>
                            <option value="郑州市轨道交通建设管理办公室"> 郑州市轨道交通建设管理办公室</option>
                            <option value="郑州仲裁委员会办公室"> 郑州仲裁委员会办公室</option>
                            <option value="市场发展局"> 市场发展局</option>
                            <option value="地震局"> 地震局</option>
                            <option value="供销合作社"> 供销合作社</option>
                            <option value="市直机关事务管理局"> 市直机关事务管理局</option>
                            <option value="住房公积金管理中心"> 住房公积金管理中心</option>
                            <option value="郑州市总工会"> 郑州市总工会</option>
                            <option value="郑州市红十字会"> 郑州市红十字会</option>
                            <option value="郑州市纪委（监察局）"> 郑州市纪委（监察局）</option>
                            <option value="民族事务管委会"> 民族事务管委会</option>
                            <option value="市供电公司"> 市供电公司</option>
                            <option value="郑州市盐业局"> 郑州市盐业局</option>
                            <option value="河南移动郑州分公司"> 河南移动郑州分公司</option>
                            <option value="中国联通河南郑州分公司"> 中国联通河南郑州分公司</option>
                            <option value="市交巡警支队"> 市交巡警支队</option>
                        </select>
                                        </span>
                            <i></i>
                        </div>
                        <div class="write-input left">
                            <em>类型：<label style="color:red">*</label></em>
                            <span>
                                <select class="MessageStyle" id="caf_write_caftype" style="width:150px" name="type">
                            <option value="" selected> 请选择类别</option>
                            <option value="0">咨询</option>
                            <option value="2">建议</option>
                            <option value="3">表扬</option>
                            <option value="4">投诉</option>
                            <option value="5">举报</option>
                            <option value="6">反弹</option>
                             </select>
                             </span>
                            <i></i>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="write-input ">
                            <em class="checkbox-em">是否公开：<label style="color:red">*</label></em>
                            <span>
                                            <div class="check-button">
                                                <input type="radio" name="opens" vld="{required:true}" id="_rdopens0"
                                                       value="1" checked/>
                                                <label for="_rdopens0">是</label>
                                            </div>
                                            <div class="check-button">
                                                <input type="radio" name="opens" vld="{required:true}" id="_rdopens1"
                                                       value="0"/>
                                                <label for="_rdopens1">否</label>
                                            </div>
                                        </span>
                            <i></i>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="write-input left">
                            <em>姓名：<label style="color:red">*</label></em>
                            <span>
									<input type="text" name="name" value="${name}"
                                           vld="{required:true,isCommonText:true}"
                                           id="caf_write_name"
                                           autocomplete="off" class="" value="" maxLength="50"/>
								</span>
                            <i></i>
                        </div>
                        <div class="write-input left">
                            <em>电话：<label style="color:red">*</label></em>
                            <span>
									<input type="text" name="phone" value="${phone}" vld="{required:true,isPhone:true}"
                                           id="caf_write_phone"
                                           autocomplete="off" class="" value="" maxLength="20"/>
								</span>
                            <i></i>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="write-input left">
                            <em>地址：<label style="color:red">*</label></em>
                            <span>
									<input type="text" name="address" value="${address}"
                                           vld="{required:true,isCommonText:true}"
                                           id="caf_write_address"
                                           autocomplete="off" class="" value="" maxLength="200"/>
								</span>
                            <i></i>
                        </div>
                        <div class="write-input left">
                            <em>邮箱：</em>
                            <span>
									<input type="text" name="email" value="${email}" vld="{required:true,email:true}"
                                           autocomplete="off"
                                           id="caf_write_email"
                                           class="inputMailList" value="" maxLength="50"/>
								</span>
                            <i></i>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="write-input ">
                            <em>内容：<label style="color:red">*</label></em>
                            <span class="textarea-span">
									<textarea name="content" autocomplete="off" vld="{required:true}"
                                              rows="5"
                                              id="caf_write_content"
                                              onkeyup="textarea_size(this,'#ccontent_7','0');" class="long-textarea"
                                              cols="30">${content}</textarea>
                                <br>
									<div class="message">您最多还可以输入<span id="ccontent_7"
                                                                       style="color:red;font-style:oblique;display:inline;">3000</span>个字</div>
                            </span>
                            <i></i>
                        </div>
                    </div>

                    <div>
                        <em>附件：</em>
                        <input type="file" name="file1" id="caf_write_upfile" contenteditable="false">
                        <span>  (注：附件格式为jpg或doc格式，附件大小不得超过5M！)
                            </span>

                    </div>
                    <br> <br>

                    <div class="write-input">
                        <em></em>
                        <span>
						<input type="text" value="${imgcode}" placeholder="输入右侧验证码" class="input" name="IMGCODE"
                               id="caf_write_IMGCODE"
                               autocomplete="off" style="width:110px;"/>
						<img id="codeImg" src="/code" onclick="caf_refreshCode()"
                             style="height:38px;vertical-align: middle;cursor:pointer;"><label
                                style="color:red">*</label>
                            <label style="color:red">${error_imgcode}</label>
					</span>
                        <i></i>
                    </div>
                    <div class="button-wrap"><a class="button" href="javascript:void(0);" onclick="form_submit()">提交</a>
                    </div>
                </form>
            </div>

        </div>


        <div class="copyright">
            <div class="main-wrap">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="right" class="copy-left">

                        </td>
                        <td width="640" class="copy-center">
                            EGOV政府网站版权所有 豫ICP备05012610号<br>
                            <span>建议使用：Chrome、Firefox、Opera、IE10及以上浏览器访问站点
                                <br>EGOV政府地址：郑州市中原中路233号 邮编：450007</span>
                        </td>
                        <td align="left" class="copy-right"><a href="http://www.zzwljc.com" target="_blank"><img
                                src="/r/cms/response/cn/images/110.png"></a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</body>

</html>