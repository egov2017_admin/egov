<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="keywords" content=""/>
    <meta http-equiv="description" content=""/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <title>EGOV网络发言人</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/caf.css"/>
</head>
<body style="background: url(${pageContext.request.contextPath}/images/bg.png)">
<div>
    <div class="main-body pubguide">
        <div class="main-wrap clearfix mt-30">
            <br>

            </br>
            <br>

            </br>
            <div class="content-box">
                <div class="box-title" center>
                    恭喜，您的留言已提交！
                    <br>
                    请保管好您的查询码，这是您再次查看该留言的唯一途径。
                    <br>
                    </br>
                    查询码：   <%=request.getAttribute("SK")%>
                    <br>
                    <img alt="二维码"
                         src="${pageContext.request.contextPath}/zxing?keycode=<%=request.getAttribute("SK")%>"></img>
                    <br>
                </div>

            </div>
        </div>
        <div class="copyright">
            <div class="main-wrap">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="right" class="copy-left">

                        </td>
                        <td width="640" class="copy-center">
                            EGOV政府网站版权所有 豫ICP备05012610号<br>
                            <span>建议使用：Chrome、Firefox、Opera、IE10及以上浏览器访问站点
                                <br>EGOV政府地址：郑州市中原中路233号 邮编：450007</span>
                        </td>
                        <td align="left" class="copy-right"><a href="http://www.zzwljc.com" target="_blank"><img
                                src="/r/cms/response/cn/images/110.png"></a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>