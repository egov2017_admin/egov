<%@ page import="bean.CAF" %>
<%@ page import="java.util.List" %>
<%@ page import="util.Db2PageUtil" %><%--
  Created by IntelliJ IDEA.
  User: dsf
  Date: 2017/9/16
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    List<CAF> cafList = (List<CAF>) request.getAttribute("cafRes");
%>
<!DOCTYPE HTML>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="keywords" content=""/>
    <meta http-equiv="description" content=""/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <title>EGOV网络发言人</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/caf.css"/>
</head>
<body style="background: url(${pageContext.request.contextPath}/images/bg.png)">
<div>
    <div class="main-body pubguide">
        <div class="main-wrap clearfix mt-30">
            <br>

            </br>
            <br>

            </br>
            <div class="content-box">
                <div class="box-title">网上咨询与反馈<a class="h-button" href="/caf?t=public">查看以往留言</a></div>
                <div class="detail-wrap">
                    <div class="attention">
                        说明：对涉及到具体工作人员的违规和服务态度的问题，请您详细提供有关情况（具体时间、地点）和您的具体联系方法，以便有关部门进行核对。如果您不希望在网上公开您的问题，
                        可以选择“不公开”选项，提交后只能凭系统反馈给您的查询码进行查询。
                    </div>
                </div>
            </div>

            <div class="detail-wrap">
                <div class="title"><span
                        class="tiplabel">主题</span>
                    <%=cafList.get(0).getCafTitle()%>
                </div>
                <br>
                <div class="date"><%=cafList.get(0).getCafPostDate()%>
                </div>
                <div class="content"><span
                        class="tiplabel">内容</span><%=cafList.get(0).getCafContent()%>
                </div>
                <div class="content"><span
                        class="tiplabel">状态</span>
                    <%=Db2PageUtil.cafSolvedStatus(cafList.get(0).getCafSolvedStatus())%>
                </div>
            </div>

            <div class="detail-wrap ">
                <div class="content"><span
                        class="tiplabel">回复内容</span>
                    <%=Db2PageUtil.cafReply(cafList.get(0).getCafReply())%>
                </div>
            </div>
            <div class="detail-wrap white-box">
                <div class="date"><%=cafList.get(0).getCafReplyDate() == null ? " " : cafList.get(0).getCafReplyDate()%>
                </div>
            </div>


        </div>
        <div class="copyright">
            <div class="main-wrap">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="right" class="copy-left">

                        </td>
                        <td width="640" class="copy-center">
                            EGOV政府网站版权所有 豫ICP备05012610号<br>
                            <span>建议使用：Chrome、Firefox、Opera、IE10及以上浏览器访问站点
                                <br>EGOV政府地址：郑州市中原中路233号 邮编：450007</span>
                        </td>
                        <td align="left" class="copy-right"><a href="http://www.zzwljc.com" target="_blank"><img
                                src="/r/cms/response/cn/images/110.png"></a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
