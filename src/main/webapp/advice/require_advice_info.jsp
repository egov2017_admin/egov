<%@ page import="bean.Mail" %>
<%@ page import="bean.RequiredAdvice" %><%--
  Created by IntelliJ IDEA.
  User: liu
  Date: 2017/9/8
  Time: 9:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    RequiredAdvice m = (RequiredAdvice)request.getAttribute("RequiredAdvice");
%>
<!DOCTYPE HTML>
<html>
<head>
    <title>市长信箱-信件书写</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/styles/mailboxWrite.css">
</head>
<body>

<div class="main-wrap clearfix mt-30">
    <div class="content-box">
        <div class="notice-list">
            <p style="text-indent: 2em; line-height: 1.5em;">
                部门:<%="税务"%>
            </p>
            <p style="text-indent: 2em; line-height: 1.5em;">
                标题:<%=m.getTitle()%>
            </p>
            <p style="text-indent: 2em; line-height: 1.5em;">
                描述:<%=m.getDescription()%>
            </p>

        </div>


        <form class="lm-form" name="mailboxForm" id="mailboxForm" action="/advice_write" method="post" >
            <input type="hidden" name="replySubject" value="<%=m.getId()%>"/>
            <div class="clearfix">
                <div class="write-input ">
                    <em>姓名:</em>
                    <span >
									<input type="text" name="name" vld="{required:true,isTitle:true}" autocomplete="off" class=" long-input" value="" maxLength="50"/>
								</span>
                    <i></i>
                </div>
            </div>
            <div class="clearfix">
                <div class="write-input ">
                    <em >电话:</em>
                    <span >
									<input type="text" name="phone" vld="{required:true,isTitle:true}" autocomplete="off" class=" long-input" value="" maxLength="50"/>
								</span>
                    <i></i>
                </div>
            </div>
            <div class="clearfix">
                <div class="write-input left">
                    <em >邮箱</em>
                    <span >
									<input type="text" name="email" vld="{required:true,isCommonText:true}" autocomplete="off" class="" value="" maxLength="50"/>
								</span>
                    <i></i>
                </div>

            </div>
            <div class="clearfix">
                <div class="write-input ">
                    <em >内容：</em>
                    <span class="textarea-span">
									<textarea name="content" autocomplete="off" vld="{required:true}" rows="5" onkeyup="calc_words()" class="long-textarea" cols="30" ></textarea>
									<div class="message">您最多还可以输入<span id="ccontent_7" style="color:red;font-style:oblique;display:inline;">3000</span>个字</div>
                                        <script type="text/javascript">
                                        function calc_words(){
                                            var content=document.getElementsByName("content")[0];
                                            if(content.value!=null&&content.value.trim().length!=0){
                                                var ccontent_7=document.getElementById("ccontent_7");
                                                if( 3000-content.value.length < 0){
                                                    alert("字符个数已超过3000字");
                                                    ccontent_7.innerHTML="0";
                                                }else {
                                                    ccontent_7.innerHTML=""+(3000-content.value.length);
                                                }

                                            }

                                        }
                                      </script>

								</span>
                    <i></i>
                </div>
            </div>

            <Script type="text/javascript">
                function check(){
                    var email=document.getElementsByName("email")[0];
                    console.log("标题"+email.value);
                    if(email.value==null || email.value.trim().length==0){
                        alert("请输入邮箱");
                        return false;
                    }else {

                    }

                    var name=document.getElementsByName("name")[0];
                    if(name.value==null || name.value.trim().length==0){
                        alert("请输入姓名");
                        return false;
                    }

                    var phone=document.getElementsByName("phone")[0];
                    if(phone.value==null || phone.value.trim().length==0||phone.value.trim().length!=7||phone.value.trim().length!=11){
                        alert("请输入电话");
                        return false;
                    }else{
                        var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
                        if(!myreg.test(phone.value)){
                            alert("输入的手机号码无效");
                            return false;
                        }
                    }
                    var content=document.getElementsByName("content")[0];
                    if(content.value==null||content.value.trim().length==0){
                        alert("请输入内容");
                        return false;
                    }else if(content.value.trim().length>3000){
                        alert("字数大于3000,请减少一些文字");
                        return false;
                    }
                    var content=document.getElementsByName("content")[0];
                    if(content.value==null||content.value.trim().length==0){
                        alert("请输入内容");
                        return false;
                    }

                }
            </script>
            <div class="button-wrap">  <input type="submit" class="button" value="提交" onclick="return check()"></div>

        </form>

    </div>
</div>

<div class="copyright">
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>