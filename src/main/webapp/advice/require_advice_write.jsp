<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>征集意见项目输入页面</title>
    <meta charset="utf-8">
</head>

<link rel="stylesheet" href="/styles/mailboxWrite.css">
<body>

<div class="main-wrap clearfix mt-30">
    <div class="content-box">

        <div class="box-title write-dp">征集意见输入页面<a class="h-button" href="/required_advice_list">查看其他征集意见项目</a></div>
        <form class="lm-form" name="mailboxForm" id="mailboxForm" action="/required_advice_write" method="post">

            <div class="clearfix">
                <div class="write-input">
                    <em >部门：<label style="color:red">*</label></em>
                    <span >
									<select name="department" vld="{required:true}">
										<option value="建议" >公安</option>
										<option value="投诉" >检察院</option>
										<option value="投诉" >检察院</option>
										<option value="举报" >法院</option>
                                        <option value="指南" >教育局</option>
										<option value="其他" >其他</option>
									</select>
                            </span>

                </div>
            </div>
            <div class="clearfix">
                <div class="write-input">
                    <em >标题：<label style="color:red">*</label></em>
                    <span >
									<input type="text" name="title" vld="{required:true,isTitle:true}" autocomplete="off" class=" long-input" value="" maxLength="50"/>
                            </span>

                </div>
            </div>
            <div class="clearfix">
                <div class="write-input">
                    <em >截止日期：<label style="color:red">*</label></em>
                    <span >
									<input type="text" name="deadline" vld="{required:true,isTitle:true}" autocomplete="off" class=" long-input" value="" maxLength="50"/>
            </span>

                </div>

            </div>



            <div class="clearfix">
                <div class="write-input">
                    <em >描述：</em>
                    <span class="textarea-span">

									<textarea name="description" autocomplete="off" vld="{required:true}" rows="5" onkeyup="calc_words();" cols="30" ></textarea>
									<div class="message">您最多还可以输入<span id="ccontent_7" style="color:red;font-style:oblique;display:inline;">3000</span>个字</div>
                                     <script type="text/javascript">
                                        function calc_words(){
                                            var description=document.getElementsByName("description")[0];
                                            if(description.value!=null&&description.value.trim().length!=0){
                                                var ccontent_7=document.getElementById("ccontent_7");
                                                if( 3000-description.value.length < 0){
                                                    alert("字符个数已超过3000字");
                                                    ccontent_7.innerHTML="0";
                                                }else {
                                                    ccontent_7.innerHTML=""+(3000-description.value.length);
                                                }
                                            }
                                        }
                                      </script>

                            </span>

                </div>
            </div>


            <Script type="text/javascript">
                function check(){
                    var title=document.getElementsByName("title")[0];

                    if(title.value==null || title.value.trim().length==0){
                        alert("请输入标题");
                        return false;
                    }
                    var deadline=document.getElementsByName("deadline")[0];
                    if(deadline.value==null||deadline.value.trim().length==0){
                        alert("请输入截止日期");
                        return false;
                    }
                    var description=document.getElementsByName("description")[0];
                    if(description.value==null||description.value.trim().length==0){
                        alert("请输入描述");
                        return false;
                    }else if(description.value.trim().length>3000){
                        alert("字数大于3000,请减少一些文字");
                        return false;
                    }

                    return true;
                }
            </script>
            <input type="submit" class="button  " value="提交" onclick="return check()">
        </form>

    </div>
</div>

<div class="copyright">
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>