<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.Mail" %>
<%@ page import="bean.RequiredAdvice" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String title=(String) request.getAttribute("title");
    Integer totalPages= (Integer) request.getAttribute("totalPages");

    if(totalPages%5==0){
        totalPages/=5;
    }else {
        totalPages/=5;
        totalPages++;
    }
    ArrayList<RequiredAdvice> requiredAdviceList=(ArrayList<RequiredAdvice>)request.getAttribute("RequiredAdviceList");

%>
<!DOCTYPE HTML>
<html>
<head>
    <title>意见征集</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/styles/mailboxWrite.css">
</head>
<body>
<div class="main-wrap clearfix mt-30">
    <div class="content-box">
        <div class="box-title">意见征项目集列表<span class="search-bar-icon"></span>
        </div>
        <form method="get" action="required_advice_list" id="searchform" class="lm-search-bar">

            <input type="text" name="title" placeholder="标题" value=<%=title%> >
            <input type="submit" value="查询">
        </form>
        <div class="lm-list">
            <%
                int len=requiredAdviceList.size();
                for(int i=0;i<len;++i){
            %>
            <ul>
                <li>
                    <a href='/required_advice_info?id=<%=requiredAdviceList.get(i).getId()%>'><%=requiredAdviceList.get(i).getTitle()%></a>
                    <p>
                        <i title="时间"><%=requiredAdviceList.get(i).getPostdate()%></i>
                        <%--<b title="部门"> <%="税务局"%></b>--%>

                        <em title="截止日期">
                            <%=requiredAdviceList.get(i).getDeadline()%></em>
                    </p>
                </li>


            </ul>
            <%
                }
            %>
            <div class="lm-list">
                <div class="page-tile">
                    <ul>
                        <%
                            for(int i=0;i<totalPages;++i){
                        %>
                        <li><a href="/required_advice_list?title=<%=title%>&pageNum=<%=(i+1)%>"><%=(i+1)%></a></li>
                        <%
                            }
                        %>
                    </ul>
                </div>
            </div>

        </div>

    </div>
</div>

<div class="copyright"  style="position:absolute;bottom:0px; width: 100%" >
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>