<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.Advice" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String title=(String) request.getAttribute("title");
    int replySubject=(Integer)request.getAttribute("replySubject");
    Integer totalPages= (Integer)request.getAttribute("totalPages");
    if(totalPages%5==0){
        totalPages/=5;
    }else {
        totalPages/=5;
        totalPages++;
    }
    ArrayList<Advice> adviceList=(ArrayList<Advice>)request.getAttribute("AdviceList");
%>
<!DOCTYPE HTML>
<html>
<head>
    <title>意见列表</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/styles/mailboxWrite.css">
</head>
<body>


<div class="main-wrap clearfix mt-30">
    <div class="content-box">
        <div class="box-title">
            <div>征集意见的项目标题:<%=title%>&nbsp&nbsp&nbsp&nbsp征集意见的项目编号:<%=replySubject%></div>
        </div>

        <form name="myForm" id="myForm" action="advice_list" method="post" class="lm-search-bar">
            <input type="hidden" name="replySubject" value="<%=replySubject%>">
            <div class="lm-list">
                <%
                    int len=adviceList.size();
                    for(int i=0;i<len;++i){
                %>
                <ul>
                    <li>

                        <p><%=adviceList.get(i).getContent()%></p>
                        <p>

                            <i title="时间">姓名:<%=adviceList.get(i).getName()%>
                            </i>
                            <b title="类型">电话:<%=adviceList.get(i).getPhone()%></b>
                            <em title="邮箱">邮箱:<%=adviceList.get(i).getEmail()%>
                            </em>
                        </p>
                    </li>
                </ul>


                <%
                    }
                %>



            </div>
        </form>
        <div class="lm-list">

            <div class="page-tile">

                <%
                    for(int i=0;i<totalPages;++i){
                %>
                <a href="/advice_list?replySubject=<%=replySubject%>&pageNum=<%=(i+1)%>"><%=(i+1)%></a>
                <%
                    }
                %>

            </div>
        </div>

    </div>
</div>

<div class="copyright" style="position:absolute;bottom:0px; width: 100%">
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>