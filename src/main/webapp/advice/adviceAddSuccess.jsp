<%--
  Created by IntelliJ IDEA.
  User: liu
  Date: 2017/9/21
  Time: 16:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>

    <title>添加意见成功</title>
    <link rel="stylesheet" href="/styles/mailboxWrite.css">

</head>
<body>
<div class="main-wrap clearfix mt-30">
    <div class="content-box">
        <div class="box-title">意见征集<a class="h-button" href="/required_advice_list">查看以往信件</a>
        </div>
        <div class="detail-wrap">
            <div class="attention"> 你对该项目的意见添加成功!
            </div>
        </div>

    </div>
</div>

<div class="copyright" style="position:absolute;bottom:0px; width: 100%">
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>