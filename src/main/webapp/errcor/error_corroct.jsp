<%--
  Created by IntelliJ IDEA.
  User: CuiHao
  Date: 2017/9/8
  Time: 11:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="jquery-3.2.1.min.js"></script>
</head>
<body>
<form action="/erroradd" method="post" enctype="multipart/form-data">
    <ul>
        <li><span>网站名称</span><input type="text" name="errname"></li>
        <li><span>错误类型</span><select name="errtype">
            <option value="0">内容无法访问</option>
            <option value="1">信息不更新</option>
            <option value="2">内容不准确</option>
            <option value="3">咨询留言不回复</option>
            <option value="4">错别字</option>
            <option value="5">虚假伪造内容</option>
            <option value="6">其他</option>
        </select></li>
        <li><span>问题页面地址</span><input type="text" name="errlink"></li>
        <li><span>截图上传</span><input type="file" name="img"></li>
        <li><span>问题描述</span><textarea name="content"></textarea></li>
        <li><span>您的姓名</span><input type="text" name="corname"></li>
        <li><span>联系电话</span><input type="text" name="corphone"></li>
        <li><span>电子邮箱</span><input type="text" name="coremile"></li>
        <li><input type="submit" value="提交"></li>
    </ul>



</form>
</body>
</html>
