<%--
  Created by IntelliJ IDEA.
  User: CuiHao
  Date: 2017/9/21
  Time: 16:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <html>
    <head>
        <title>Title</title>
        <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="form.js"></script>
        <script type="text/javascript" src="page-manager.js"></script>
        <script type="text/javascript">
            function action(erid,path){
                var myform = document.getElementById('myform');
                var id = document.getElementById('erId');
                id.value = erid;
                myform.action = path;
                myform.submit();
            };
        </script>
    </head>
<body>
<form id="myform" method="post" >
    <input type="hidden" id="erId" name="erId">
</form>
<table>
    <tr>
        <th>序号</th>
        <th>网站名称</th>
        <th>问题类型</th>
        <th>曝光日期</th>
        <th>详细信息</th>
        <th>状态操作</th>
        <th>删除操作</th>
    </tr>
    <c:forEach var = "error" items="${res}" varStatus="status">
        <tr>
            <th>${status.index + 1}</th>
            <th>${error.ersiteName}</th>
            <th>${error.erType}</th>
            <th>${error.erDate}</th>
            <th>
                <a href="javascript:action('${error.erId}','/detail')">查看</a>
            </th>
            <th>${error.erSolvedStatus}</th>
            <th><a href="javascript:action('${error.erId}','/delete')">删除</a></th>

        </tr>
    </c:forEach>
</table>
<a href="javascript:jump('first')">首页</a>
<a href="javascript:jump('pre')">上一页</a>
<a>第${page}页/共${pagenum}页</a>
<a href="javascript:jump('next')">下一页</a>
<a href="javascript:jump('last')">尾页</a>
<form id="pageform" action="/errorlist" method="post">
    <input type="hidden" id="page" name="page" value="${page}">
    <input type="hidden" id="pagenum" name="pagenum" value="${pagenum}">
</form>
</body>
</html>
</head>
<body>

</body>
</html>
