<%--
  Created by IntelliJ IDEA.
  User: CuiHao
  Date: 2017/9/20
  Time: 0:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript">
    </script>
</head>
<body>
    <span>
        <h2>网站名称</h2>
        <h2>${res.get(0).getErsiteName()}</h2>
    </span><span>
        <h2>错误类型</h2>
        <h2>${res.get(0).getErType()}</h2>
    </span>
    <span>
        <h2>网站链接</h2>
        <h2>${res.get(0).getErLink()}</h2>
    </span>
    <span>
        <h2>错误截图</h2>
        <img src="/upload/${res.get(0).getErSCR()}">
    </span>
    <span>
        <h2>具体描述</h2>
        <h2>${res.get(0).getErContent()}</h2>
    </span>
    <span>
        <h2>提交人姓名</h2>
        <h2>${res.get(0).getName()}</h2>
    </span>
    <span>
        <h2>提交时间</h2>
        <h2>${res.get(0).getErDate()}</h2>
    </span>
    <button onclick="window.history.back()">返回</button>
</body>
</html>
