<%@ page import="bean.CAF" %>
<%@ page import="java.util.List" %>
<%@ page import="util.Db2PageUtil" %>
<%@ page import="util.StringUtil" %>
<%
    List<CAF> cafList = (List<CAF>) request.getAttribute("cafRes");
    String type = (String) request.getAttribute("type");
    int pagenum = 1;
    pagenum = (int) request.getAttribute("pagenum");
    String curpage = (String) request.getAttribute("page");
    //  int curpage = Integer.parseInt(curPage);
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/caf.js">
    </script>
    <title>EGOV网络发言人</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/caf.css"/>
</head>
<body style="background: url(${pageContext.request.contextPath}/images/bg.png)">
<div>
    <div class="main-body pubguide">
        <div class="main-wrap clearfix mt-30">
            <br>

            </br>
            <br>

            </br>
            <div class="content-box">
                <div class="box-title" center>网上咨询与反馈后台管理</div>
                <form method="get" action="/cafm" id="cafm_searchform" class="lm-search-bar">
                    <input type="hidden" name="t" value="search">
                    <input type="hidden" name="page" id="cafm_page"
                           value="<%=request.getAttribute("page")==null?1:request.getAttribute("page")%>">
                    <input type="text" name="title" value="${title}" placeholder="标题">
                    <select class="MessageStyle" id="type" style="width:150px" name="type">
                        <option value="" selected> 请选择类别</option>
                        <%if (StringUtil.isNotEmpty(type)) {%>
                        <% for (int i = 0; i < 6; i++) { %>
                        <option value="<%=i%>" <%if (type.equals(i + "")) {%>
                                selected<%}%>><%=Db2PageUtil.cafType(i + "")%>
                        </option>
                        <%}%>
                        <%} else {%>
                        <option value="0">咨询</option>
                        <option value="1">建议</option>
                        <option value="2">表扬</option>
                        <option value="3">投诉</option>
                        <option value="4">举报</option>
                        <option value="5">反弹</option>
                        <%}%>
                    </select>
                    <select class="MessageStyle" id="status" style="width:100px" name="status">
                        <%--<option value="" selected> 未解决</option>--%>
                        <option value="0" selected>未解决</option>
                        <option value="1">已解决</option>
                        <option value="2">已交办</option>
                    </select>
                    <input type="image" src="${pageContext.request.contextPath}/images/search.png"
                           onclick="cafm_search_form_submit()"
                           style="margin-left: 20px;
	                        border: none;
	                        background: #60D4D4;
	                        height: 36px;
	                        padding: 0 15px;
	                        color: white;
	                        border-radius: 4px;
	                        cursor: pointer;
	                        -webkit-appearance:none;
	                        vertical-align:middle;"/>
                </form>
                <div class="lm-list">
                    <ul>
                        <%if (cafList.size() == 0) {%>
                        <li style="cursor:default;"><p><a href="javascript:void(0);">无信件！</a></p></li>
                        <%}%>
                        <% for (int i = 0; i < cafList.size(); i++) { %>
                        <li>
                            <a href="/cafm?t=detail&id=<%=cafList.get(i).getCafId()%>">【<%=cafList.get(i).getCafId()%>】<%=cafList.get(i).getCafTitle()%>
                            </a>
                            <p>
                                <i title="时间"><%=cafList.get(i).getCafPostDate()%>
                                </i>
                                <b title="类型"><%=Db2PageUtil.cafType(cafList.get(i).getCafType())%>
                                </b>
                                <em title="状态"><%=Db2PageUtil.cafSolvedStatus(cafList.get(i).getCafSolvedStatus())%>
                                </em></p>
                        </li>
                        <%} %>
                    </ul>
                </div>
                <div class="page-tile">
                    <span></span>
                    <%--<%if (curpage >= 2) {%>--%>
                    <%--<a href="javascript:caf_split('prev')" class="prev"></a>--%>
                    <%--<%}%>--%>
                    <%if (pagenum > 1) {%>
                    <% for (int i = 1; i <= pagenum; i++) { %>
                    <%if (curpage.equals((i + ""))) {%>
                    <span class="focus"><%=i%></span>
                    <%} else {%>
                    <a href="javascript:cafm_split('select','<%=i%>')"><%=i%>
                    </a>
                    <%}%>
                    <%--<%if (curpage == i) {%>--%>
                    <%--<span class="focus"><%=curpage%></span>--%>
                    <%--<%}%>--%>
                    <%}%>
                    <%}%>
                    <%--<a href="javascript:split('select')"></a>--%>
                    <%--<a href="search_3.jspx?id=80&seacode=&seatitle=&seatype=">3</a>--%>
                    <%--<a href="search_4.jspx?id=80&seacode=&seatitle=&seatype=">4</a>--%>
                    <%--<a href="search_5.jspx?id=80&seacode=&seatitle=&seatype=">5</a>--%>
                    <%--<a href="search_6.jspx?id=80&seacode=&seatitle=&seatype=">6</a>--%>
                    <%--<a href="search_7.jspx?id=80&seacode=&seatitle=&seatype=">7</a>--%>
                    <%--&lt;%&ndash;<%if (pagenum >= 8) {%>&ndash;%&gt;--%>
                    <%--&lt;%&ndash;<span class="more"></span>&ndash;%&gt;--%>
                    <%--&lt;%&ndash;<%}%>&ndash;%&gt;--%>
                    <%--<a href="search_10.jspx?id=80&seacode=&seatitle=&seatype=">10</a>--%>
                    <%--<%if (pagenum >= 2 && curpage != pagenum) {%>--%>
                    <%--<a href="javascript:caf_split('next')" class="next"></a>--%>
                    <%--<%}%>--%>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="main-wrap">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="right" class="copy-left">

                        </td>
                        <td width="640" class="copy-center">
                            EGOV政府网站版权所有 豫ICP备05012610号<br>
                            <span>建议使用：Chrome、Firefox、Opera、IE10及以上浏览器访问站点
                                <br>EGOV政府地址：郑州市中原中路233号 邮编：450007</span>
                        </td>
                        <td align="left" class="copy-right"><a href="http://www.zzwljc.com" target="_blank"><img
                                src="/r/cms/response/cn/images/110.png"></a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>