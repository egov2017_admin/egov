<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="bean.CAF" %>
<%@ page import="java.util.List" %>
<%@ page import="util.Db2PageUtil" %>
<%@ page import="util.StringUtil" %><%--
  Created by IntelliJ IDEA.
  User: dsf
  Date: 2017/9/16
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    List<CAF> cafList = (List<CAF>) request.getAttribute("cafRes");
%>
<!DOCTYPE HTML>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="keywords" content=""/>
    <meta http-equiv="description" content=""/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/caf.js">
    </script>

    <script src="${pageContext.request.contextPath}/js/jquery.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/analytics.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/less.version.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.placeholder.min.js" type="text/javascript"></script>

    <title>EGOV网络发言人后台管理</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/caf.css"/>
</head>
<body style="background: url(${pageContext.request.contextPath}/images/bg.png)">
<div>
    <div class="main-body pubguide">
        <div class="main-wrap clearfix mt-30">
            <br>

            </br>
            <br>

            </br>
            <div class="content-box">
                <div class="box-title">添加回复<a class="h-button" href="/cafm?t=delete&id=<%=cafList.get(0).getCafId()%>&filename=<%=cafList.get(0).getCafFile()==null?"":cafList.get(0).getCafFile()%>"
                                              onclick="javascript:return reply_del()">删除</a>
                </div>
                <div class="detail-wrap">
                    <div class="attention">
                        说明：对涉及到具体工作人员的违规和服务态度的问题，请您详细提供有关情况（具体时间、地点）和您的具体联系方法，以便有关部门进行核对。如果您不希望在网上公开您的问题，
                        可以选择“不公开”选项，提交后只能凭系统反馈给您的查询码进行查询。
                    </div>
                </div>
            </div>

            <div class="detail-wrap">
                <div class="title"><span
                        class="tiplabel">主题</span><%=cafList.get(0).getCafTitle()%>
                </div>
                <br>
                <div class="date"><%=cafList.get(0).getCafPostDate()%>
                </div>
                <div class="content"><span
                        class="tiplabel">办理单位</span><%=cafList.get(0).getDepId()%>
                </div>
                <div class="content"><span
                        class="tiplabel">类型</span><%=Db2PageUtil.cafType(cafList.get(0).getCafType())%>
                </div>
                <div class="content"><span
                        class="tiplabel">是否公开</span><%=Db2PageUtil.cafPublic(cafList.get(0).getCafPublic())%>
                </div>
                <div class="content"><span
                        class="tiplabel">姓名</span><%=cafList.get(0).getName()%>
                </div>
                <div class="content"><span
                        class="tiplabel">电话</span><%=cafList.get(0).getPhone()%>
                </div>
                <div class="content"><span
                        class="tiplabel">地址</span><%=cafList.get(0).getAddress()%>
                </div>
                <div class="content"><span
                        class="tiplabel">邮箱</span><%=cafList.get(0).getEmail()%>
                </div>
                <div class="content"><span
                        class="tiplabel">内容</span><%=cafList.get(0).getCafContent()%>
                </div>
                <div class="content"><span
                        class="tiplabel">附件</span><%=Db2PageUtil.cafFileName(cafList.get(0).getCafFile())%>
                    <%if (StringUtil.isNotEmpty(cafList.get(0).getCafFile())) {%>
                    <a href="/download?filename=<%=cafList.get(0).getCafFile()%>"> 点击下载</a>
                    <%}%>
                </div>

            </div>
            <input type="hidden" id="reply_textarea_bak"
                   value="<%=cafList.get(0).getCafReply() == null ? " " : cafList.get(0).getCafReply()%>">
            <form method="get" action="/cafm" id="replyform" name="replyform">
                <input type="hidden" name="t" value="reply">
                <input type="hidden" name="id"
                       value="<%=cafList.get(0).getCafId()%>">
                <%--<div class="detail-wrap">--%>
                <%--<div class="content"><span--%>
                <%--class="tiplabel">更改处理状态</span>--%>
                <%--<select class="MessageStyle" id="status" style="width:100px" name="status">--%>
                <%--&lt;%&ndash;<option value="" selected> 未解决</option>&ndash;%&gt;--%>
                <%--<option value="1">已解决</option>--%>
                <%--<option value="2" selected>已交办</option>--%>
                <%--</select>--%>
                <%--</div>--%>
                <%--</div>--%>
                <div class="detail-wrap">
                    <div class="content"><span
                            class="tiplabel">更改处理状态</span>
                        <div class="clearfix">
                            <div class="write-input ">
                                        <span>
                                            <div class="check-button">
                                                <input type="radio" name="status" vld="{required:true}" id="_rdopens0"
                                                       value="1" checked/>
                                                <label for="_rdopens0">已解决</label>
                                            </div>
                                            <div class="check-button">
                                                <input type="radio" name="status" vld="{required:true}" id="_rdopens1"
                                                       value="2"/>
                                                <label for="_rdopens1">已交办</label>
                                            </div>
                                        </span>
                                <i></i>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="detail-wrap">
                    <div class="content"><span
                            class="tiplabel">回复内容</span>
                        <div class="clearfix">
                            <div class="write-input ">
                                <span class="textarea-span">
									<textarea id="reply_textarea" name="content" autocomplete="off"
                                              vld="{required:true}"
                                              rows="5" onchange="reply_textarea_change(this.id)"
                                              onkeyup="textarea_size(this,'#ccontent_7','0');" class="long-textarea"
                                              cols="30"><%=cafList.get(0).getCafReply() == null ? " " : cafList.get(0).getCafReply()%></textarea>
									<div class="message">您最多还可以输入<span id="ccontent_7"
                                                                       style="color:red;font-style:oblique;display:inline;">3000</span>个字
                                    </div>
                                     <div class="date">
                                         <%=cafList.get(0).getCafReplyDate() == null ? " " : "回复日期：" + cafList.get(0).getCafReplyDate()%>
                                     </div>
                                 </span>
                            </div>
                        </div>

                        <i></i>
                    </div>
                </div>

                <br>
                <br>
                <div class="button-wrap"><a class="button" href="javascript:void(0);" onclick="reply_submit()">提交</a>
                </div>
            </form>
        </div>
    </div>
    <div class="copyright">
        <div class="main-wrap">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td align="right" class="copy-left">

                    </td>
                    <td width="640" class="copy-center">
                        EGOV政府网站版权所有 豫ICP备05012610号<br>
                        <span>建议使用：Chrome、Firefox、Opera、IE10及以上浏览器访问站点
                                <br>EGOV政府地址：郑州市中原中路233号 邮编：450007</span>
                    </td>
                    <td align="left" class="copy-right"><a href="http://www.zzwljc.com" target="_blank"><img></a></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</div>
</body>
</html>