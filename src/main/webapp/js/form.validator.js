//是否是空字符串
function isempty(v) {
    return !v || $.trim(v).length == 0;
}
function isInteger(string) {
    var integer;

    integer = parseInt(string);
    if (isNaN(integer)) {
        return false;
    }
    if (integer.toString().length != string.length)
        return false;
    return true;
}

var conStr="";
// 是否不包含某一个字符串
function isContains(s) {
	var patrn = /(admin)|(null)|(sex)|(super)|(fuck)|(shit)|(bitch)|(艹)/i;
	var regexp =  /(admin)|(null)|(sex)|(super)|(fuck)|(shit)|(bitch)|(艹)/gi;
	conStr = s.match(regexp);
	return patrn.test(s);
}

// 是否是固定电话
function isTel(s) {
	var patrn = /^(\d{3,4}-?)?\d{8}$/ ;
	return patrn.test(s);
}
// 是否是手机号码
function isMobile(s) {
    var patrn = /^1\d{10}$/;
    return patrn.test(s);
}
// 是否是电话号码
function isPhone(s){
    return isMobile(s)||isTel(s);
}

// 是否是信箱
function isEmail(strValue) {
    return /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(strValue);
}
// 是否是title
function isTitle(strValue) {
	return /^[0-9a-zA-Z\u4e00-\u9fa5\.\-@_,，\.。!！？“”]+$/.test(strValue);
}
// 是否是title
function isCommonText(strValue) {
	return /^[0-9a-zA-Z\u4e00-\u9fa5\.\-@_]+$/.test(strValue);
}

// 是否是数字
function isDigit(strValue) {
    return /^[0-9]+$/.test(strValue);
}
// 是否是邮编
function isZipcode(strValue) {
    return /^(\d){6}$/.test(strValue);
}
// 是否是暂住证
function isZZZcode(strValue) {
    return /^(\d){15,18}$/.test(strValue);
}
// 是否是中文
function isChn(strValue) {
    return /^[\u4E00-\u9FA5\uF900-\uFA2D]+$/.test(strValue);
}
// 是否是网址
function isUrl(strValue) {
    return /^(file|http|https|ftp|mms|telnet|news|wais|mailto):\/\/(.+)$/.test(strValue);
}
// 验证日期
function isDate(s) {
    var patrn = /^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[0-9])|([1-2][0-3]))\:([0-5]?[0-9])((\s)|(\:([0-5]?[0-9])))))?$/;
    return patrn.test(s);
}

function isCnName(sValue){
	return /^([\u4E00-\u9FA5])+(·[\u4E00-\u9FA5]+)*$/.test(sValue);
}

// 验证性别是否是男
function getGender(sValue){
	var isMan = "1";// 男
	if(isCnCardId(sValue)){
		var sConvertedValue = $.trim(sValue).toLowerCase();
		if (sConvertedValue.length == 18) {
			sConvertedValue = sConvertedValue.substring(0, 17);
		}
		var flag=sConvertedValue.substr(sConvertedValue.length-1,1);
		if (flag == "x" || flag == "X") {
			flag = "10";
		}
		var f = parseInt(flag);
		var j = f % 2;
		if (j === 0)
			return "0";// 女
	}
	return isMan;
}

// 获取身份证上的出生日期
function getBirth(sValue){
	if(isCnCardId(sValue)){
		var sConvertedValue = $.trim(sValue).toLowerCase();
	    if (sConvertedValue.length == 15)
	    	sConvertedValue = sConvertedValue.substring(0, 6) + "19" + sConvertedValue.substr(6, 9);

	    var sYear = sConvertedValue.substr(6, 4);
	    var sMonth = sConvertedValue.substr(10, 2);
	    var sDay = sConvertedValue.substr(12, 2);

	    return sYear + "-" + sMonth + "-" + sDay;
	}
	return "1900-01-01";
}

// 验证二代身份证
function isCnCardId(sValue) {
    var bIsValid = false;
    var oVerifyCode = "1,0,x,9,8,7,6,5,4,3,2".split(",");
    var oWi = "7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2".split(",");
    var oChecker = "1,9,8,7,6,5,4,3,2,1,1".split(",");
    var sConvertedValue = $.trim(sValue).toLowerCase();

    if (sConvertedValue.length != 18)
        return bIsValid;

    var sNoCheckPlace = sConvertedValue.substring(0, 17);
    if (isNaN(sNoCheckPlace))
        return bIsValid;

    var sYear = sNoCheckPlace.substr(6, 4);
    var sMonth = sNoCheckPlace.substr(10, 2);
    var sDay = sNoCheckPlace.substr(12, 2);

    if (!isDate(sYear + "-" + sMonth + "-" + sDay))
        return bIsValid;

    var oBirthday = new Date(sYear, sMonth, sDay);
    var oMinDate = new Date(1900, 1, 1);
    var oMaxDate = new Date();

    if (oBirthday < oMinDate || oBirthday > oMaxDate)
        return bIsValid;

    var iTotalAiWi = 0;

    for (var ix = 0; ix < 17; ++ix) {
        iTotalAiWi += parseInt(sNoCheckPlace.substr(ix, 1)) * oWi[ix];
    }

    var iModedValue = -1;
    iModedValue = iTotalAiWi % 11;

    var sValidValue = sNoCheckPlace + oVerifyCode[iModedValue];

    if (sValidValue != sConvertedValue)
        return bIsValid;

    bIsValid = true;

    return bIsValid;
}

jQuery.validator.addMethod("isCnCardId",
		function(value, element,params) {
			return this.optional(element) || isCnCardId(value);
		},
		"请输入正确的身份证号码");
jQuery.validator.addMethod("isCnName",
		function(value, element,params) {
			return this.optional(element) || isCnName(value);
		},
		"请输入中文姓名");
jQuery.validator.addMethod("isUnContains",
		function(value, element,params) {
			params[0]=conStr;
			return this.optional(element) || !isContains(value);
		},
		$.format("不允许出现“{0}”字符"));
jQuery.validator.addMethod("isTel",
		function(value, element,params) {
			return this.optional(element) || isTel(value);
		},
		"请输入正确的固话号码");
jQuery.validator.addMethod("isMobile",
		function(value, element,params) {
			return this.optional(element) || isMobile(value);
		},
		"请输入11位手机号码");
jQuery.validator.addMethod("isZipcode",
		function(value, element,params) {
			return this.optional(element) || isZipcode(value);
		},
		"请输入6位邮政编码");
jQuery.validator.addMethod("isPhone",
		function(value, element,params) {
	return this.optional(element) || isPhone(value);
},
"必须为正确的电话号码格式");
jQuery.validator.addMethod("isTitle",
		function(value, element,params) {
	return this.optional(element) || isTitle(value);
},
"只允许字母、数字、中文字符");
jQuery.validator.addMethod("isCommonText",
		function(value, element,params) {
	return this.optional(element) || isCommonText(value);
},
"只允许字母、数字、中文字和'_','-','@'");