// function cafm_split(tag, val) {
//     var page = document.getElementById("cafm_page");
//     if (tag = "prev") page.value = val - 1;
//     if (tag = "next") page.value = val + 1;
//     document.cafm_searchform.submit();
// }

// function cafm_split_const(val) {
//     var page = document.getElementById("cafm_page");
//     page.value = val;
//     document.cafm_searchform.submit();
// }

function cafm_split(tag, val) {
    var page = document.getElementById("cafm_page");
    if (tag == "select") {
        page.value = val;
        cafm_searchform.submit();
    }
}

function reply_del() {
    var msg = "您真的确定要删除吗？\n\n请确认！";
    if (confirm(msg) == true) {
        return true;
    } else {
        return false;
    }
}

function search_form_submit() {
    var page = document.getElementById("caf_page");
    var sk = document.getElementById("caf_search_sk");
    if (sk.value == "查询码不存在！") sk.value = null;
    page.value = 1;
    document.caf_searchForm.submit();
}

function cafm_search_form_submit() {
    var page = document.getElementById("cafm_page");
    page.value = 1;
    document.cafm_searchForm.submit();
}

function caf_split(tag, val) {
    var page = document.getElementById("caf_page");
    var sk = document.getElementById("caf_search_sk");
    if (sk.value == "查询码不存在！") sk.value = null;
    if (tag == "select") {
        page.value = val;
        //   document.getElementById("caf_type").value
        caf_searchform.submit();
    }
}

function form_submit() {
    if (caf_write_checkform()) {
        form.submit();
    }
    return;
}

function reply_submit() {
    if (reply_textarea_change("reply_textarea")) {
        if (isValueChange("reply_textarea", "reply_textarea_bak")) {
            replyform.submit();
            alert("成功：回复已提交！");
        } else alert("失败：并未更改回复内容，无需提交！");

    }
    else alert("失败：回复字数不合法！（为0或超出限制）");
    return;
}

function reply_textarea_change(id) {
    var text = document.getElementById(id).value;
    if (text.length > 3000) return false;
    if (text.length == 0) return false;
    else return true;
}

function isValueChange(id1, id2) {
    var id_1 = document.getElementById(id1).value;
    var id_2 = document.getElementById(id2).value;
    if (id_1 == id_2) return false;
    else return true;
}

function textarea_size(par, content, maxLen) {
    var max = 3000;
    if (!isNaN(maxLen) && maxLen != 0)
        max = maxLen;
    if ($(par).val().length < max)
        $(content).html(max - $(par).val().length);
    else
        $(content).html(0);
}

//刷新验证码
function caf_refreshCode() {
    var codeImg = document.getElementById("codeImg");
    var d = new Date();
    codeImg.src = "/code?s=" + d;
}

function caf_write_checkform() {
    var title = document.getElementById("caf_write_title").value;
    if (title.replace(/\s/g, "") == "") {
        alert("请输入主题");
        return false;
    }

    if (title.length > 50) {
        alert("请输入50个字以内的标题 ");
        return false;
    }
    var name = document.getElementById("caf_write_name").value;
    if (name.replace(/\s/g, "") == "") {
        alert("请输入姓名");
        return false;
    }
    var re = /[^u4e00-u9fa5]/;
    if (!re.test(name)) {
        alert("姓名请输入中文");
        return false;
    }

    var phone = document.getElementById("caf_write_phone").value;
    if (phone.replace(/\s/g, "") == "") {
        alert("请输入电话");
        return false;
    }

    if (phone.length < 8) {
        alert("电话：请输入大于7位数字");
        return false;
    }

    if (re.test(phone)) {
        alert("电话：请输入数字");
        return false;
    }


    var address = document.getElementById("caf_write_address").value;
    if (address.replace(/\s/g, "") == "") {
        alert("请输入地址");
        return false;
    }

    var email = document.getElementById("caf_write_email").value;
    if (email.replace(/\s/g, "") == "") {
        alert("请输入邮箱");
        return false;
    }
    var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    if (!myreg.test(email)) {
        alert('请输入有效的E_mail！');
        document.getElementById("email").focus();
        return false;
    }

    var content = document.getElementById("caf_write_content").value;
    if (content.replace(/\s/g, "") == "") {
        alert("请输入内容");
        return false;
    }
    if (content.length > 3000 || content.length < 5) {
        alert("内容必须在5-3000个字之内");
        return false;
    }

    var file1 = document.getElementById("caf_write_upfile").value;
    var str = file1.replace(/\s/g, "");
    if (str != "") {
        var filetype = str.substring(str.lastIndexOf('.'), str.length).toLowerCase();
        if (filetype != '.jpg' && filetype != '.doc') {
            alert("上传的文件格式不正确");
            return false;
        }
    }

    var imagevalue = document.getElementById("caf_write_IMGCODE").value;
    if (imagevalue.replace(/\s/g, "") == "") {
        alert("请输入验证码！");
        return false;
    }

    var qtyp = document.getElementById("caf_write_caftype").value;
    if (qtyp.replace(/\s/g, "") == "") {
        alert("请选择信件类别");
        return false;
    }


    var orgid = document.getElementById("caf_write_depid").value;
    if (orgid.replace(/\s/g, "") == "") {
        alert("请选择办理单位");
        return false;
    }
    return true;
}