function jump(obj) {
    var page = document.getElementById('page');
    var pagenum = document.getElementById('pagenum');
    var pageform = document.getElementById('pageform');
    switch (obj){
        case 'first':
            page.value = '1';
            break;
        case 'pre':
            var temp;
            if(page.value=='1'){
                alert('已经是第一页了');
                return true;
            } else{
                temp = parseInt(page.value)-1;
                page.value=temp;
            }
            break;
        case 'next':
            page.value = parseInt(page.value)+1;
            break;
        case 'last':
            page.value = '-1';
            break;
    }
    pageform.submit();
    return true;
}