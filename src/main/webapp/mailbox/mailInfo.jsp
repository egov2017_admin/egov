<%@ page import="bean.Mail" %><%--
  Created by IntelliJ IDEA.
  User: liu
  Date: 2017/9/8
  Time: 9:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Mail m = (Mail)request.getAttribute("Mail");
%>
<!DOCTYPE HTML>
<html>
<head>
    <title>市长信箱-信件阅读</title>
    <link rel="stylesheet" href="/styles/mailboxWrite.css">

</head>

<body>
<div class="main-wrap clearfix mt-30">
    <div class="content-box">
        <div class="box-title">市长信箱<a class="h-button" href="/mailSearchServlet">查看以往信件</a>
        </div>
        <div class="detail-wrap">
            <div class="attention">说明：对涉及到具体工作人员的违规和服务态度的问题，请您详细提供有关情况（具体时间、地点）和您的具体联系方法，以便有关部门进行核对。如果您不希望在网上公开您的问题， 可以选择“不公开”选项，提交后只能凭系统反馈给您的查询码进行查询。
            </div>
        </div>
        <div class="detail-wrap">
            <div class="title"><span class="tiplabel">主题</span><%=m.getMailTitle()%>
            </div>
            <div class="date">
                <%=m.getPostdate().toString()%>
            </div>
        </div>
        <div class="detail-wrap">
            <div class="content">
                <span class="tiplabel">内容</span><%=m.getContent()%>
            </div>

        </div>

        <div class="detail-wrap">
            <div class="status">
                <%--<span class="tiplabel">办理状态</span>--%>
                <%
                    String status;

                    if(m.getReply()!=null&&m.getReply().length()>0){
                        status="办理完毕";
                    }else{
                        status="未完毕";
                    }
                %>
              <%=status%>
            </div>

        </div>







    <div class="detail-wrap white-box">
        <div class="reply">
            <font>
                <%
                    String reply;
                    if(m.getReply()!=null&&m.getReply().length()>0){
                        reply=m.getReply();
                    }else{
                        reply="有关部门还未回复";
                    }
                %>
                <%=reply%>
            </font>
            <div class="date"><%=m.getReplydate()!=null ? "回复时间为:"+m.getReplydate().toString():"回复时间为:未回复"%></div>
        </div>
    </div>
    </div>
</div>
<div class="copyright">
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
        </body>
        </html>