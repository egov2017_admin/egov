<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.Mail" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String SearchKey= (String) request.getAttribute("SearchKey");
    if(SearchKey==null) SearchKey="";
%>
<!DOCTYPE HTML>
<html>
<head>

    <title>市长信箱-查询码</title>
    <link rel="stylesheet" href="/styles/mailboxWrite.css">

</head>
<body>
<div class="main-wrap clearfix mt-30">
    <div class="content-box">
        <div class="box-title">市长信箱<a class="h-button" href="/mailSearchServlet">查看以往信件</a>
        </div>
        <div class="detail-wrap">
            <div class="attention"> 如果选择了“不公开”选项，提交后只能凭系统反馈给您的查询码进行查询。
            </div>
        </div>
        <div class="detail-wrap">
            <div class="title"><span class="tiplabel">你的查询码:</span><%=SearchKey%>
            </div>

        </div>
        <div class="box-title"><a class="h-button" href="/mailSearchServlet?searchKey=<%=SearchKey%>">去查询</a>
        </div>

    </div>
</div>

<div class="copyright" style="position:absolute;bottom:0px; width: 100%">
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>