<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.Mail" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    String title=(String) request.getAttribute("title");
    if(title==null) title="";
    Integer totalPages= (Integer) request.getAttribute("totalPages");
    String search_type=(String)request.getAttribute("search_type");
    String typeStr[]={"建议","投诉","举报","指南","其他"};
    if(totalPages%5==0){
        totalPages/=5;
    }else {
        totalPages/=5;
        totalPages++;
    }
    ArrayList<Mail> mailList=(ArrayList<Mail>)request.getAttribute("mailList");

%>
<!DOCTYPE HTML>
<html>
<head>
    <title>市长信箱-回复信件</title>
    <meta charset="utf-8">

    <link rel="stylesheet" href="/styles/mailboxWrite.css">
</head>
<body>
<div class="main-wrap clearfix mt-30">
    <div class="content-box">
        <div class="box-title">市长信箱-回信<span class="search-bar-icon"></span>
        </div>
        <form method="get" action="mailReplyListServlet" id="searchform" class="lm-search-bar">

            <input type="text" name="title" placeholder="标题" value=<%=title%> >
            <select name="search_type">
                <option value="">请选择</option>
                <%
                    for(int i=0;i<typeStr.length;++i){
                        if(typeStr[i].equals(search_type)){
                %>
                <option value="<%=typeStr[i]%>" selected="selected"><%=typeStr[i]%></option>
                <%
                }else {
                %>
                <option value="<%=typeStr[i]%>" ><%=typeStr[i]%></option>
                <%
                        }
                    }
                %>
            </select>
            <input type="submit" value="查询">
        </form>
        <div class="lm-list">
            <%
                int len=mailList.size();
                for(int i=0;i<len;++i){
            %>
            <ul>
                <li>
                    <a href='/mailInfoServlet?id=<%=mailList.get(i).getMailId()%>'><%=mailList.get(i).getMailTitle()%></a>
                    <p>
                        <i title="时间">时间:<%=mailList.get(i).getPostdate().toString()%></i>
                        <b title="类型">类型: <%

                            int type=mailList.get(i).getMailType();
                        %>
                            <%=typeStr[type]%></b>
                        <em title="状态">状态:
                            <%
                                String status;

                                if(mailList.get(i).getReply()!=null&&mailList.get(i).getReply().length()>0){
                                    status="办理完毕";
                                }else{
                                    status="未完毕";
                                }
                            %>
                            <%=status%></em>
                    </p>
                    <p>
                    <form  action="mailReplyListServlet" method="post">
                        <input type="hidden" name="replyId" value=<%=mailList.get(i).getMailId()%> ></text><%
                            if(mailList.get(i).getReply() != null&&mailList.get(i).getReply().trim().length()!=0){
                        %><textarea style="margin: 0px; width: 984px; height: 117px;" name="replyContent"  ><%=mailList.get(i).getReply().trim()%><%
                    }else {
                        %><textarea style="margin: 0px; width: 984px; height: 117px;" name="replyContent"><%
                        }
                    %></textarea>

                            <Script type="text/javascript">
                            function check(form){
                                if(form.replyContent.value.trim()==""){
                                    alert("请输入回复内容!");
                                    form.replyContent.focus();
                                    return false;
                                }
                                return true;
                            }
                              </script>
            <input type="submit"  class="button" value="提交回复" onclick="return check(this.form)">
                    </form>

                    </p>
                </li>
            </ul>
            <%
                }
            %>
            <div class="lm-list">

                <div class="page-tile">

                    <%
                        for(int i=0;i<totalPages;++i){
                    %>
                    <a href="/mailReplyListServlet?title=<%=title%>&search_type=<%=search_type%>&pageNum=<%=(i+1)%>"><%=(i+1)%>
                    </a>
                    <%
                        }
                    %>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="copyright">
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>