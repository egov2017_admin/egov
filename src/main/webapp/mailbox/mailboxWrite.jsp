<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>市长信箱-信件书写</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/styles/mailboxWrite.css">
</head>
<body>

<div class="main-wrap clearfix mt-30">
    <div class="content-box">
        <div class="box-title write-dp">写信说明<a class="h-button" href="/mailSearchServlet">查看以往信件</a></div>
        <div class="notice-list"><p style="text-indent: 2em; line-height: 1.5em;">1、对郑州市改革开放、经济建设、城市管理方面的意见和建议和设想；</p><p style="text-indent: 2em; line-height: 1.5em;">2、对政府各部门工作职责、办事程序和政策规定的咨询、意见和建议；</p><p style="text-indent: 2em; line-height: 1.5em;">3、对政府工作的批评、意见和建议；</p><p style="text-indent: 2em; line-height: 1.5em;">4、对关系群众切身利益、实际困难的求助；</p><p style="text-indent: 2em; line-height: 1.5em;">5、属于行政部门应当处理的其他有关问题的意见和建议；</p><p style="text-indent: 2em; line-height: 1.5em;">6、请要求回复者须留下准确的联系方式，以便我们及时与您沟通；</p><p style="text-indent: 2em; line-height: 1.5em;">7、受理申请公开服务。</p><p style="text-indent: 2em; line-height: 1.5em;">注：如果您不希望在网上公开您的问题，请选择“不公开”选项，随后凭系统反馈给您的查询码进行查询。</p><p style="text-indent: 2em; line-height: 1.5em;">如果您不希望您的个人信息向信件具体办理单位公开<span style="line-height: 1.5em; text-indent: 2em;">，请选择“隐私保护”。</span></p></div>
        <a class="write-dp-link mt-20" href="write.jspx?p=2&id=80">点击查看写信说明</a>
        <form class="lm-form" name="mailboxForm" id="mailboxForm" action="/mailWriteServlet" method="post" >
            <input type="hidden" name="mbid" value="80">
            <div class="clearfix">
                <div class="write-input ">
                    <em >类型：<label style="color:red">*</label></em>
                    <span >
									<select name="type" vld="{required:true}">
                                    <option value="建议" >建议</option>
                                    <option value="投诉" >投诉</option>
                                    <option value="举报" >举报</option>
                                    <option value="指南" >指南</option>
                                    <option value="其他" >其他</option>
									</select>
								</span>
                    <i></i>
                </div>
            </div>
            <div class="clearfix">
                <div class="write-input ">
                    <em >标题:</em>
                    <span >
									<input type="text" name="title" vld="{required:true,isTitle:true}" autocomplete="off" class=" long-input" value="" maxLength="50"/>
								</span>
                    <i></i>
                </div>
            </div>
            <div class="clearfix">
                <div class="write-input left">
                    <em >姓名：</em>
                    <span >
									<input type="text" name="name" vld="{required:true,isCommonText:true}" autocomplete="off" class="" value="" maxLength="50"/>
								</span>
                    <i></i>
                </div>
                <div class="write-input left">
                    <em >电话：</em>
                    <span >
									<input type="text" name="phone" vld="{required:true,isPhone:true}" autocomplete="off" class="" value="" maxLength="20"/>
								</span>
                    <i></i>
                </div>
            </div>
            <div class="clearfix">
                <div class="write-input left">
                    <em >地址：</em>
                    <span >
									<input type="text" name="address" vld="{required:true,isCommonText:true}" autocomplete="off" class="" value="" maxLength="200"/>
								</span>
                    <i></i>
                </div>
                <div class="write-input left">
                    <em >邮箱：</em>
                    <span >
									<input type="text" name="email" vld="{required:true,email:true}" autocomplete="off" class="inputMailList" value="" maxLength="50"/>
								</span>
                    <i></i>
                </div>
            </div>
            <div class="clearfix">
                <div class="write-input ">
                    <em class="checkbox-em">是否公开：<label style="color:red">*</label></em>
                    <span >
									<div class="check-button">
										<input type="radio" name="opens" vld="{required:true}" id="_rdopens0" value="是" checked/>
										<label for="_rdopens0">是</label>
									</div>
									<div class="check-button">
										<input type="radio" name="opens" vld="{required:true}" id="_rdopens1" value="否" />
										<label for="_rdopens1">否</label>
									</div>
								</span>
                    <i></i>
                </div>
            </div>
            <div class="clearfix">
                <div class="write-input ">
                    <em >内容：</em>
                    <span class="textarea-span">
									<textarea name="content" autocomplete="off" vld="{required:true}" rows="5" onkeyup="calc_words()" class="long-textarea" cols="30" ></textarea>
									<div class="message">您最多还可以输入<span id="ccontent_7" style="color:red;font-style:oblique;display:inline;">3000</span>个字</div>
                                        <script type="text/javascript">
                                        function calc_words(){
                                            var content=document.getElementsByName("content")[0];
                                            if(content.value!=null&&content.value.trim().length!=0){
                                                var ccontent_7=document.getElementById("ccontent_7");
                                                if( 3000-content.value.length < 0){
                                                    alert("字符个数已超过3000字");
                                                    ccontent_7.innerHTML="0";
                                                }else {
                                                    ccontent_7.innerHTML=""+(3000-content.value.length);
                                                }

                                            }

                                        }
                                      </script>

								</span>
                    <i></i>
                </div>
            </div>

            <Script type="text/javascript">
                function check(){
                    var title=document.getElementsByName("title")[0];
                    console.log("标题"+title.value);
                    if(title.value==null || title.value.trim().length==0){
                        alert("请输入标题");
                        return false;
                    }

                    var name=document.getElementsByName("name")[0];
                    if(name.value==null || name.value.trim().length==0){
                        alert("请输入姓名");
                        return false;
                    }

                    var phone=document.getElementsByName("phone")[0];
                    if(phone.value==null || phone.value.trim().length==0){
                        alert("请输入电话");
                        return false;
                    }
                    var address=document.getElementsByName("address")[0];
                    if(address.value==null||address.value.trim().length==0){
                        alert("请输入地址");
                        return false;
                    }
                    var content=document.getElementsByName("content")[0];
                    if(content.value==null||content.value.trim().length==0){
                        alert("请输入内容");
                        return false;
                    }else if(content.value.trim().length>3000){
                        alert("字数大于3000,请减少一些文字");
                        return false;
                    }
                    var content=document.getElementsByName("content")[0];
                    if(content.value==null||content.value.trim().length==0){
                        alert("请输入内容");
                        return false;
                    }
                    var content = document.getElementsByName("captcha")[0];
                    if(content.value==null||content.value.trim().length==0){
                        alert("请输入验证码");
                        return false;
                    }
                }
            </script>
            <div class="button-wrap">  <input type="submit" class="button" value="提交" onclick="return check()"></div>

        </form>

    </div>

</div>

<div class="copyright">
    <div class="main-wrap">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
                <td align="right" class="copy-left">
                    <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js' type='text/javascript'%3E%3C/script%3E"));</script><span id="_ideConac"><a href="//bszs.conac.cn/sitename?method=show&amp;id=119BFFD0C8762DAFE053012819ACA6BB" target="_blank"><img id="imgConac" vspace="0" hspace="0" border="0" src="//dcs.conac.cn/image/red_error.png" data-bd-imgshare-binded="1"></a></span><script src="http://dcs.conac.cn/js/17/251/0000/40880056/CA172510000408800560001.js" type="text/javascript"></script><span id="_ideConac"></span>
                </td>
                <td width="640" class="copy-center">
                    郑州市人民政府版权所有 豫ICP备05012610号<br>
                    <span>建议使用：IE8及以上浏览器 郑州市人民政府地址：郑州市中原中路233号 邮编：450007</span>
                </td>
                <td align="left" class="copy-right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>