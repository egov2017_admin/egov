package dao;

import bean.Admin;
import bean.Mail;
import util.DruidUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by liu on 2017/9/22.
 */
public class AdminDao {
    Connection conn=null;
    public int check(String username,String password){
        if(username==null||username.length()==0||username.length()>10){
            return -1;
        }
        if(password==null||password.length()==0||password.length()>10){
            return -1;
        }
        try {
            conn= DruidUtil.getConnection();
            String sql="select * from egov_admin where admUserName= ? and admPassword = ?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setString(1,username);
            ps.setString(2,password);
            ResultSet rs=ps.executeQuery();
            Admin admin=new Admin();
           while (rs.next()){
               admin.setAdmId(rs.getInt("admId"));
               admin.setAdmUserName(rs.getString("admUserName"));
               admin.setAdmType(rs.getInt("admType"));
           }
            if(admin.getAdmUserName()==null||admin.getAdmUserName().length()==0){
               return -1;
            }
            return admin.getAdmType();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }


    public int getAdminListCount(String userName, String search_type) {
        StringBuffer sql=new StringBuffer();
        int search_type_int=0;
        String typeStr[]={"超级管理员","市长信箱管理员","咨询反馈管理员"};
        for(int i=0;i<typeStr.length;++i){
            if (typeStr[i].equals(search_type)) search_type_int = i;
        }
        sql.append("select count(*) from egov_admin where   1=1");
        if(userName!=null&&userName.length()!=0){
            sql.append("  and  admUserName like ? ");
        }
        if(search_type.length()!=0){
            sql.append(" and  admType = ? ");
        }
        int count=0;
        try {
            conn= DruidUtil.getConnection();
            PreparedStatement ps=conn.prepareStatement(sql.toString());
            int index=1;
            if(userName!=null&&userName.length()!=0){
                ps.setString(index,"%"+userName+"%");
                index++;
            }
            if(search_type.length()!=0){
                ps.setInt(index,search_type_int);
                index++;
            }
            ResultSet rs=ps.executeQuery();

            while(rs.next()){
                count=rs.getInt(1);
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public ArrayList<Admin> getAdminList(String userName, String search_type, int pageNum) {
        ArrayList<Admin> adminList =new ArrayList<Admin>();
        int k=(pageNum-1)*5;
        StringBuffer sql=new StringBuffer();
        int search_type_int=0;
        String typeStr[]={"超级管理员","市长信箱管理员","咨询反馈管理员"};
        for(int i=0;i<typeStr.length;++i){
            if (typeStr[i].equals(search_type)) search_type_int = i;
        }
        sql.append("select * from egov_admin where   1=1");
        if(userName!=null&&userName.length()!=0){
            sql.append("  and  admUserName like ? ");
        }
        if(search_type.length()!=0){
            sql.append(" and  admType = ? ");
        }

        sql.append(" limit "+k+",5");
        try {
            conn= DruidUtil.getConnection();
            PreparedStatement ps=conn.prepareStatement(sql.toString());
            int index=1;
            if(userName!=null&&userName.length()!=0){
                ps.setString(index,"%"+userName+"%");
                index++;
            }
            if(search_type.length()!=0){
                ps.setInt(index,search_type_int);
                index++;
            }
            ResultSet rs=ps.executeQuery();
            while(rs.next()){
                Admin a=new Admin();
                a.setAdmId(rs.getInt("admId"));
                a.setAdmUserName(rs.getString("admUserName"));
                a.setAdmType(rs.getInt("admType"));
                adminList.add(a);
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return adminList;
    }

    public boolean modifyUserInfo(Admin a) {
        StringBuffer sql=new StringBuffer("update egov_admin ");
        int index=1;
        if(a.getAdmUserName()!=null&&a.getAdmUserName().length()!=0){
            sql.append("set admUserName = ? ");
            index++;
        }
        if(a.getAdmPassword()!=null&&a.getAdmPassword().length()!=0){
            if(index==1){
                sql.append("set admPassword = ? ");
            }else {
                sql.append(", admPassword = ? ");
            }
            index++;
        }
        if(a.getAdmType()!=-1){
            if(index==1){
                sql.append("set admType = ? ");
            }else {
                sql.append(", admType = ? ");
            }
            index++;
        }
        sql.append(" where admId = ?");
        try {
            conn= DruidUtil.getConnection();
            PreparedStatement ps=conn.prepareStatement(sql.toString());
            int pos=1;
            if(a.getAdmUserName()!=null&&a.getAdmUserName().length()!=0){
                ps.setString(pos,a.getAdmUserName());
                pos++;
            }
            if(a.getAdmPassword()!=null&&a.getAdmPassword().length()!=0){
                ps.setString(pos,a.getAdmPassword());
                pos++;
            }
            if(a.getAdmType()!=-1){
                ps.setInt(pos,a.getAdmType());
                pos++;
            }
            ps.setInt(pos,a.getAdmId());
            boolean result=ps.execute();
            conn.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
