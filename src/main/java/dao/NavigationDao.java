package dao;

import bean.Navigation;
import util.Connect;
import util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 站点导航的DAO【增删改，一览】
 *
 * @author dsf
 * @date|time 2017/9/8|23:09
 * @last update:
 */
public class NavigationDao {

    /**
     * 导航条目增加
     *
     * @param navigation
     * @return
     * @throws Exception
     */
    public int navigationAdd(Navigation navigation, Connect conn) throws Exception {
        List<Object> para = new ArrayList<Object>();
//
        String sql = "insert into egov_navigation values( ?, ?, ?, ?, ?)";
        para.add(navigation.getNavId());
        para.add(navigation.getNavName());
        para.add(navigation.getNavLink());
        para.add(navigation.getNavType());
        para.add(navigation.getNavPostDate());
        System.out.println(sql);
        return conn.update(sql, para);
    }

    /**
     * 导航站点删除（by navid）
     *
     * @param navId
     * @return
     * @throws Exception
     */
    public int navigationDelete(String navId, Connect conn) throws Exception {
        String sql = "delete from egov_navigation where navId= ？";
        List<Object> para = new ArrayList<Object>();
        para.add(navId);
        System.out.println(sql);
        return conn.update(sql, para);
    }

    /**
     * 导航站点修改
     *
     * @param navigation
     * @return
     * @throws Exception
     */
    public int navigationModify(Navigation navigation, Connect conn) throws Exception {
        StringBuffer sb = new StringBuffer("update egov_navigation set navName=?,navLink=?,navType=?,navPostDate=? where navid=?");
        List<Object> para = new ArrayList<Object>();

        para.add(navigation.getNavName());
        para.add(navigation.getNavLink());
        para.add(navigation.getNavType());
        para.add(navigation.getNavPostDate());
        para.add(navigation.getNavId());

        String sql = sb.toString();
        System.out.println(sql);
        return conn.update(sql, para);
    }

    /**
     * 站点导航一览
     * 获取navName,navLink
     *
     * @return
     * @throws Exception
     */
    public List<Navigation> navSiteList(Navigation navigation, Connect conn) throws Exception {
        StringBuffer sb = new StringBuffer("select * from egov_navigation where navType= ?");
        String sql = sb.toString();
        List<Object> para = new ArrayList<Object>();
        para.add(navigation.getNavType());
        System.out.println(sql);
        ArrayList<Navigation> navRes = (ArrayList) conn.queryForArrObject(sql, para, Navigation.class);

        return navRes;
    }

    /**
     * 部门导航一览
     *
     * @param navigation
     * @param conn
     * @return
     * @throws Exception
     */
    public List<Navigation> navDepList(Navigation navigation, Connect conn) throws Exception {
        StringBuffer sb = new StringBuffer("select * from egov_navigation where navType= ?");
        String sql = sb.toString();
        List<Object> para = new ArrayList<Object>();
        para.add("1");
        System.out.println(sql);
        ArrayList<Navigation> navRes = (ArrayList) conn.queryForArrObject(sql, para, Navigation.class);

        return navRes;
    }

}