package servlet;

import dao.MailDao;
import dao.RequiredAdviceDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name="required_advice_list",urlPatterns = "/required_advice_list")
public class required_advice_list extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title=req.getParameter("title");

        String pageNum=req.getParameter("pageNum");
        if(pageNum==null){
            pageNum="1";
        }
        if(title==null){
            title="";
        }
        RequiredAdviceDao dao= null;
        try {
            dao = new RequiredAdviceDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("title",title);
        req.setAttribute("totalPages",dao.getRequiredAdviceListCount(title));
        req.setAttribute("RequiredAdviceList",dao.getRequiredAdviceList(title,Integer.parseInt(pageNum)));
        req.getRequestDispatcher("/advice/require_advice_list.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
