package servlet;

import bean.Mail;
import dao.MailDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by liu on 2017/9/20.
 */
@WebServlet(name="mailDeleteServlet",urlPatterns = "/mailDeleteServlet")
public class mailDeleteServlet extends HttpServlet{

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        MailDao mailDao= null;

            mailDao = new MailDao();
        String mails[]=req.getParameterValues("check");//用数组接受前台传来的被选中的复选框的值
        if(mails!=null){
            int m=0;
            for(int i=0;i<mails.length;i++){//循环删除被选中的数据
                int id=Integer.parseInt(mails[i]);
                mailDao.deleteMailById(id);
            }
        }

        String pageNum=req.getParameter("pageNum");
        if(pageNum==null){
            pageNum="1";
        }

        req.setAttribute("totalPages",mailDao.getMailListCount("",""));
        req.setAttribute("mailList",mailDao.getMailList("","",Integer.parseInt(pageNum)));
        req.getRequestDispatcher("/mailbox/mailDelete.jsp").forward(req,resp);
    }
}
