package servlet;

import bean.RequiredAdvice;
import dao.AdviceDao;
import dao.RequiredAdviceDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name="advice_list",urlPatterns = "/advice_list")
public class advice_list extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int replySubject=Integer.parseInt( req.getParameter("replySubject"));
        AdviceDao dao= null;

            dao = new AdviceDao();


        String pageNum=req.getParameter("pageNum");
        if(pageNum==null){
            pageNum="1";
        }
        String title = null;
        try {
            RequiredAdviceDao requiredAdviceDao=new RequiredAdviceDao();
            RequiredAdvice ra=requiredAdviceDao.getRequiredAdviceById(replySubject);
            title=ra.getTitle();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("title",title);
        req.setAttribute("replySubject",replySubject);
        req.setAttribute("totalPages",dao.getAdviceListCount(replySubject));
        req.setAttribute("AdviceList",dao.getAdviceList(replySubject,Integer.parseInt(pageNum)));
        req.getRequestDispatcher("/advice/advice_list.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
