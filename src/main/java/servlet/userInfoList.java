package servlet;

import bean.Admin;
import dao.AdminDao;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name="userInfoList",urlPatterns = "/userInfoList")
public class userInfoList extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String search_type=req.getParameter("search_type");
        String search_name=req.getParameter("search_name");
        String pageNum=req.getParameter("pageNum");
        String UserName=req.getParameter("UserName");
        String Password=req.getParameter("Password");
        String Type=req.getParameter("Type");
        String id=req.getParameter("id");
        AdminDao dao= null;

        dao = new AdminDao();
          if(id!=null&&id.length()!=0){
                Admin a=new Admin();
                a.setAdmId(Integer.valueOf(id));
                a.setAdmUserName(UserName);
                a.setAdmPassword(Password);
                if(Type==null||Type.length()==0){
                    a.setAdmType(-1);
                }else {
                    int type_int=-1;
                    String typeStr[]={"超级管理员","市长信箱管理员","咨询反馈管理员"};
                    for(int i=0;i<typeStr.length;++i){
                        if (typeStr[i].equals(Type)) type_int = i;
                    }

                    a.setAdmType(type_int);
                }
                dao.modifyUserInfo(a);
        }
        if(pageNum==null){
            pageNum="1";
        }
        if(search_name==null){
            search_name="";
        }
        if(search_type==null){
            search_type="";
        }

        req.setAttribute("search_name",search_name);
        req.setAttribute("search_type",search_type);
        req.setAttribute("totalPages",dao.getAdminListCount(search_name,search_type));
        req.setAttribute("userInfoList",dao.getAdminList(search_name,search_type,Integer.parseInt(pageNum)));
        req.getRequestDispatcher("/adm/userInfoList.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
