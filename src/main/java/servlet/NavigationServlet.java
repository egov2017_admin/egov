package servlet;

import bean.Navigation;
import dao.NavigationDao;
import util.Connect;
import util.JdbcUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 导航服务对应的servlet
 * doPost 增加导航项
 * doGet 导航项一览
 * doPut 修改导航想
 * doDelete 删除导航项
 *
 * @author dsf
 * @date|time 2017/9/11|19:25
 * @update:
 */
@WebServlet(name = "NavigationServlet", urlPatterns = "/navigation")
public class NavigationServlet extends HttpServlet {
    NavigationDao navigationDao = new NavigationDao();

    /**
     * doPost 增加导航项
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        //id自增置为0
        String navId = "0";
        String navName = "河南";
        String navLink = "henan.net";
        String navType = "0";

        Connect con = JdbcUtils.getInstance();
        Navigation navSitePara = new Navigation();

        navSitePara.setNavId(navId);
        navSitePara.setNavName(navName);
        navSitePara.setNavLink(navLink);
        navSitePara.setNavType(navType);
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH/mm/ss");
        navSitePara.setNavPostDate(df.format(new Date()));

        try {
            int  navRes = navigationDao.navigationAdd(navSitePara, con);
            if (navRes != 1) {
                resp.getWriter().write("error");
            } else {
                System.out.println("添加站点条目成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * doGet 导航项一览
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        Connect con = Connect.getInstance();
        Navigation navSitePara = new Navigation();
        navSitePara.setNavType("0");
        Navigation navDepPara = new Navigation();
        navDepPara.setNavType("1");
        List<Navigation> navSiteRes, navDepRes;
        try {
            //从站点导航表取得所有关于站点的导航
            navSiteRes = navigationDao.navSiteList(navSitePara, con);
            System.out.println(navSiteRes.size());
            for (Navigation nav : navSiteRes) {
                System.out.println(nav.getNavName() + " " + nav.getNavLink() + " " + nav.getNavPostDate());
            }
            navDepRes = navigationDao.navSiteList(navDepPara, con);
            System.out.println(navDepRes.size());
            for (Navigation nav : navSiteRes) {
                System.out.println(nav.getNavName() + " " + nav.getNavLink() + " " + nav.getNavPostDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * doPut 修改导航项
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        String navId = "0";
        String navName = "河南";
        String navLink = "henan.net";
        String navType = "0";
        System.out.println("________进入doPut 修改导航项_______");
        Connect con = JdbcUtils.getInstance();
        Navigation navPara = new Navigation();
        navPara.setNavId(navId);
        navPara.setNavName(navName);
        navPara.setNavLink(navLink);
        navPara.setNavType(navType);

        try {
            int navRes = navigationDao.navigationModify(navPara, con);
            if (navRes != 0) {
                System.out.println("修改成功");
            }
            System.out.println("修改失败");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * doDelete 删除导航项
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        //获取所需删除条目的信息
        String navId = "2";

        Connect con = Connect.getInstance();
        try {
            //从站点导航表删除id对应条目
            int navSiteRes = navigationDao.navigationDelete(navId, con);
            if (navSiteRes != 1) {
                resp.getWriter().write("error");
            } else {
                System.out.println("删除站点条目成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
