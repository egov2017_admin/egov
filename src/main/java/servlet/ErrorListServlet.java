package servlet;

import bean.ErrorBean;
import util.Connect;

import javax.jws.WebService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import bean.ErrorBean;

@WebServlet(name="errorlistservlet",urlPatterns ={"/errorlist"})
public class ErrorListServlet extends HttpServlet{

    static int page = 1;
    static int pageint = 10;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        page = new DoList().errorList(request,response,page,pageint,"/errorList.jsp");
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
