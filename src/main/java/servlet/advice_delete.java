package servlet;

import bean.RequiredAdvice;
import dao.AdviceDao;
import dao.RequiredAdviceDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name="advice_delete",urlPatterns = "/advice_delete")
public class advice_delete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int replySubject=Integer.parseInt( req.getParameter("replySubject"));
        AdviceDao dao= null;

            dao = new AdviceDao();

        String advices[]=req.getParameterValues("check");//用数组接受前台传来的被选中的复选框的值
        if(advices!=null){
            int m=0;
            for(int i=0;i<advices.length;i++){//循环删除被选中的数据
                int id=Integer.parseInt(advices[i]);
                m=dao.deleteAdviceById(replySubject,id);
            }
        }

        String pageNum=req.getParameter("pageNum");
        if(pageNum==null){
            pageNum="1";
        }
        String title = null;
        try {
            RequiredAdviceDao requiredAdviceDao=new RequiredAdviceDao();
            RequiredAdvice ra=requiredAdviceDao.getRequiredAdviceById(replySubject);
            title=ra.getTitle();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("title",title);
        req.setAttribute("replySubject",replySubject);
        req.setAttribute("totalPages",dao.getAdviceListCount(replySubject));
        req.setAttribute("AdviceList",dao.getAdviceList(replySubject,Integer.parseInt(pageNum)));
        req.getRequestDispatcher("/advice/advice_delete.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
