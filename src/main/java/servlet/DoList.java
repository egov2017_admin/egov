package servlet;

import bean.ErrorBean;
import util.Connect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class DoList extends HttpServlet {
    public int errorList(HttpServletRequest request, HttpServletResponse response,int page ,int pageint ,String path) throws ServletException, IOException {
        String sql1 = "select count(1) from egov_error_correction";
        Connect conn = Connect.getInstance();
        int sum = conn.count(sql1,null);
        int pagenum = (sum%10 == 0) ? sum/10 : sum/10+1;
        if (request.getParameter("page") != null && request.getParameter("page") != "")
            if("-1".equals(request.getParameter("page")))
                page = pagenum;
            else
                page = new Integer(request.getParameter("page"));
        if(page > pagenum)
            page = pagenum;

        String sql2 = "SELECT t1.erId, t1.ersiteName, t1.erType, t1.erLink, t1.erSCR, t1.erContent, t1.name, t1.erDate " +
                "FROM (SELECT t.erId, t.ersiteName, t.erType, t.erLink, t.erSCR, t.erContent, t.name, t.erDate " +
                "FROM egov.egov_error_correction t ORDER BY t.erId DESC) t1 LIMIT ";
//        List<Object> prams = new ArrayList<Object>();
        sql2 = sql2 + (page-1)*pageint + "," +pageint ;
//        prams.add((page-1)*pageint);
//        prams.add(pageint);
        List<ErrorBean> res = (List)conn.queryForArrObject(sql2,null,ErrorBean.class);
        for(ErrorBean bean:res){
            switch(new Integer(bean.getErType())){
                case 0:bean.setErType("内容无法访问");break;
                case 1:bean.setErType("信息不更新");break;
                case 2:bean.setErType("内容不准确");break;
                case 3:bean.setErType("咨询留言不回复");break;
                case 4:bean.setErType("错别字");break;
                case 5:bean.setErType("虚假伪造内容");break;
                case 6:bean.setErType("其他");break;
                default:bean.setErType("未知错误");
            }
        }
        for(ErrorBean bean:res)
            bean.setErSolvedStatus(bean.getErSolvedStatus()=="0"?"未处理":"已处理");
        request.setAttribute("res",res);
        request.setAttribute("page",page);
        request.setAttribute("pagenum",pagenum);
        request.getRequestDispatcher("/errcor/"+path).forward(request,response);
        return page;
    }

}
