package servlet;

import bean.Mail;
import dao.MailDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by liu on 2017/9/8.
 */
@WebServlet(name="mailInfoServlet",urlPatterns = "/mailInfoServlet")
public class mailInfoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        MailDao dao= null;

            dao = new MailDao();

        Mail m=dao.getMailById(Integer.parseInt(id));
        req.setAttribute("Mail",m);
        req.getRequestDispatcher("/mailbox/mailInfo.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
