package servlet;
import bean.RequiredAdvice;
import dao.RequiredAdviceDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by liu on 2017/9/8.
 */
@WebServlet(name="required_advice_info",urlPatterns = "/required_advice_info")
public class required_advice_info extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        RequiredAdviceDao dao= null;
        try {
            dao = new RequiredAdviceDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        RequiredAdvice m=dao.getRequiredAdviceById(Integer.parseInt(id));
        req.setAttribute("RequiredAdvice",m);
        req.getRequestDispatcher("/advice/require_advice_info.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
