package servlet;

import bean.Advice;
import dao.AdviceDao;
import dao.RequiredAdviceDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name="advice_write",urlPatterns = "/advice_write")
public class advice_write extends HttpServlet{
    @Override
    protected  void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Advice m=new Advice();
        int replySubject=Integer.parseInt(req.getParameter("replySubject"));
        m.setReplySubject(replySubject);

        String content=req.getParameter("content");
        m.setContent(content);

        String name=req.getParameter("name");
        m.setName(name);

        String phone=req.getParameter("phone");
        m.setPhone(phone);

        String email=req.getParameter("email");
        m.setEmail(email);

        AdviceDao dao= null;

            dao = new AdviceDao();
        try {
            dao.addAdvice(m);
        } catch (Exception e) {
            e.printStackTrace();
        }

        resp.sendRedirect("/advice/adviceAddSuccess.jsp");
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}