//package servlet;
//
//import bean.ErrorBean;
//
//import util.Connect;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
//@WebServlet(name="errorservlet",urlPatterns ={"/erroradd"})
//public class ErrorServer extends HttpServlet{
//
//    @Override
//    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//        SmartUpload su = new SmartUpload();
//        su.initialize(this.getServletConfig(),request,response);//初始化环境
//        su.setAllowedFilesList("jpg,png,jpeg,gif,JPG,PNG,JPEG,GIF");//设置允许文件类型
//        String filename = "";
//        try {
//            // 设置拒绝类型
//            su.setDeniedFilesList("exe,doc,txt,EXE,DOC,TXT");
//            su.upload();
//            int cnt = su.getFiles().getCount();
//            for (int i = 0; i < cnt; i++) {
//                File fl = su.getFiles().getFile(i);
//                if (!fl.isMissing()) {
//                    filename = fl.getFileName();
//                    // 上传文件到web应用的 /upload/文件夹下
//                    filename = "/upload/" + filename;
//                    fl.saveAs(filename, SmartUpload.SAVE_VIRTUAL);
//                }
//            }
//        } catch (SQLException e) {
//
//        } catch (SmartUploadException e) {
//            e.printStackTrace();
//        }
//
//        Request req = su.getRequest();//获得由su转换的httprequest对象
//
//        ErrorBean bean = new ErrorBean();
//        bean.setErsiteName(req.getParameter("errname"));
//        bean.setErType(req.getParameter("errtype"));
//        bean.setErLink(req.getParameter("errlink"));
//        bean.setErContent(req.getParameter("content"));
//        bean.setName(req.getParameter("corname"));
//        bean.setPhone(req.getParameter("corphone"));
//        bean.setEmail(req.getParameter("coremile"));
//        String sql = "insert into egov_error_correction(ersiteName, erType, erLink, erSCR, erContent, name, phone, email) " +
//                "values (?,?,?,?,?,?,?,?)";
//        List<Object> params = new ArrayList<Object>();
//        params.add(bean.getErsiteName());
//        params.add(bean.getErType());
//        params.add(bean.getErLink());
//        params.add(filename);
//        params.add(bean.getErContent());
//        params.add(bean.getName());
//        params.add(bean.getPhone());
//        params.add(bean.getEmail());
//
//        Connect conn = Connect.getInstance();
//        if (conn.update(sql, params) < 1)
//            request.getRequestDispatcher("/errcor/error.jsp").forward(request, response);
//        else
//            request.getRequestDispatcher("/errcor/success.jsp").forward(request, response);
//
//
//
//
//
//
//            /*try {
//                if(request.getParameter("name")!=null||request.getParameter("name")!="")
//                {
//                    request.setAttribute("test_sd","nonono");
//                    request.getRequestDispatcher("error_corroct.jsp").forward(request,response);
//                }
//                else {
//                    request.setAttribute("test_sd","ok");
//                    request.getRequestDispatcher("error_corroct.jsp").forward(request,response);
//                }
//                System.out.println(request.getParameter("name"));
//            } catch (ServletException e) {
//                e.printStackTrace();
//            }
//            request.setAttribute("test","成功");*/
//
//        return;
//    }
//    @Override
//    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//        doPost(request,response);
//    }
//}
