package servlet;

import dao.MailDao;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name="mailReplyListServlet",urlPatterns = "/mailReplyListServlet")
public class mailReplyListServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title=req.getParameter("title");
        String search_type=req.getParameter("search_type");
        String pageNum=req.getParameter("pageNum");
        String replyContent=req.getParameter("replyContent");
        String replyId=req.getParameter("replyId");
        MailDao dao= null;

            dao = new MailDao();


        if(replyContent!=null&&replyContent.length()!=0){
                replyContent.trim();
                if(replyContent.length()!=0){
                    dao.setReply(Integer.parseInt(replyId),replyContent);
                }
        }
        if(pageNum==null){
            pageNum="1";
        }
        if(title==null){
            title="";
        }
        if(search_type==null){
            search_type="";
        }

        req.setAttribute("title",title);
        req.setAttribute("search_type",search_type);
        req.setAttribute("totalPages",dao.getMailListCountBackend(title,search_type));
        req.setAttribute("mailList",dao.getMailListBackend(title,search_type,Integer.parseInt(pageNum)));
        req.getRequestDispatcher("/mailbox/mailReplyList.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
