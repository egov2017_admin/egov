package servlet;

import bean.RequiredAdvice;
import dao.RequiredAdviceDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name="required_advice_write",urlPatterns = "/required_advice_write")
public class required_advice_write extends HttpServlet{
    @Override
    protected  void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequiredAdvice m=new RequiredAdvice();
        String department=req.getParameter("department");
        int depid=1;
        m.setDepid(depid);
        String title=req.getParameter("title");
        m.setTitle(title);
        String description=req.getParameter("description");
        m.setDescription(description);
        String deadline=req.getParameter("deadline");
        m.setDeadline(deadline);


        RequiredAdviceDao dao= null;
        try {
            dao = new RequiredAdviceDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            dao.addRequiredAdvice(m);
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.sendRedirect("/required_advice_list?title="+title);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}