package servlet;

import bean.ErrorBean;
import util.Connect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name="detailservlet",urlPatterns ={"/detail"})
public class Detail extends HttpServlet{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("erId");
        String sql = "SELECT ersiteName, erType, erLink, erSCR, erContent, name, erDate FROM egov_error_correction WHERE erId = " + id;
        Connect conn = Connect.getInstance();
        List<ErrorBean> res = (List)conn.queryForArrObject(sql,null,ErrorBean.class);
        switch(new Integer(res.get(0).getErType())){
            case 0:res.get(0).setErType("内容无法访问");break;
            case 1:res.get(0).setErType("信息不更新");break;
            case 2:res.get(0).setErType("内容不准确");break;
            case 3:res.get(0).setErType("咨询留言不回复");break;
            case 4:res.get(0).setErType("错别字");break;
            case 5:res.get(0).setErType("虚假伪造内容");break;
            case 6:res.get(0).setErType("其他");break;
            default:res.get(0).setErType("未知错误");
        }
        res.get(0).setErSolvedStatus(res.get(0).getErSolvedStatus()=="0"?"未处理":"已处理");
        request.setAttribute("res",res);
        request.getRequestDispatcher("/errcor/detail.jsp").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
