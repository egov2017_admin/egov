package bean;

/**
 * Created by liu on 2017/9/22.
 */
public class Admin {
    private int admId;
    private String admUserName;
    private String admPassword;
    private int admType;

    public int getAdmId() {
        return admId;
    }

    public void setAdmId(int admId) {
        this.admId = admId;
    }

    public String getAdmUserName() {
        return admUserName;
    }

    public void setAdmUserName(String admUserName) {
        this.admUserName = admUserName;
    }

    public String getAdmPassword() {
        return admPassword;
    }

    public void setAdmPassword(String admPassword) {
        this.admPassword = admPassword;
    }

    public int getAdmType() {
        return admType;
    }

    public void setAdmType(int admType) {
        this.admType = admType;
    }
}
