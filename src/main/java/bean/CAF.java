package bean;

/**
 * 咨询反馈类
 *
 * @author dsf
 * @Date|time 9.10|15:13
 * @update:change some name for DB changes
 */
public class CAF {

    //id
    private String cafId;
    //主题/标题
    private String cafTitle;
    //咨询反馈部门号
    private String depId;
    //类型（0咨询；1建议；2表扬；3投诉；4举报；5反弹；）
    private String cafType;
    //是否公开
    private String cafPublic;
    //姓名
    private String name;
    //电话
    private String phone;
    //住址
    private String address;
    //电邮
    private String email;
    //内容
    private String cafContent;
    //上传文件
    private String cafFile;
    //提交日期
    private String cafPostDate;
    //解决时间
    private String cafReplyDate;
    //解决状态（0 未解决；1已解决 ）
    private String cafSolvedStatus;
    //回复内容
    private String cafReply;
    //查询码
    private String cafSearchKey;

    public String getCafId() {
        return cafId;
    }

    public void setCafId(String cafId) {
        this.cafId = cafId;
    }

    public String getCafTitle() {
        return cafTitle;
    }

    public void setCafTitle(String cafTitle) {
        this.cafTitle = cafTitle;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    public String getCafType() {
        return cafType;
    }

    public void setCafType(String cafType) {
        this.cafType = cafType;
    }

    public String getCafPublic() {
        return cafPublic;
    }

    public void setCafPublic(String cafPublic) {
        this.cafPublic = cafPublic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCafContent() {
        return cafContent;
    }

    public void setCafContent(String cafContent) {
        this.cafContent = cafContent;
    }

    public String getCafFile() {
        return cafFile;
    }

    public void setCafFile(String cafFile) {
        this.cafFile = cafFile;
    }

    public String getCafPostDate() {
        return cafPostDate;
    }

    public void setCafPostDate(String cafPostDate) {
        this.cafPostDate = cafPostDate;
    }

    public String getCafReplyDate() {
        return cafReplyDate;
    }

    public void setCafReplyDate(String cafReplyDate) {
        this.cafReplyDate = cafReplyDate;
    }

    public String getCafSolvedStatus() {
        return cafSolvedStatus;
    }

    public void setCafSolvedStatus(String cafSolvedStatus) {
        this.cafSolvedStatus = cafSolvedStatus;
    }

    public String getCafReply() {
        return cafReply;
    }

    public void setCafReply(String cafReply) {
        this.cafReply = cafReply;
    }

    public String getCafSearchKey() {
        return cafSearchKey;
    }

    public void setCafSearchKey(String cafSearchKey) {
        this.cafSearchKey = cafSearchKey;
    }
}