package bean;

/**
 * @author dsf
 * @date|time 2017/9/18|0:44
 * @update:
 */
public class Page {
    //当前第几页
    private int current;
    //每页最大条目数
    private int size;
    //起始页
    private int start;

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }
}
