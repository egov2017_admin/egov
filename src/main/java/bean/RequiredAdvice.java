package bean;

/**
 * Created by liu on 2017/9/9.
 */
public class RequiredAdvice {
  private int id;
  private String title;
  private String description;
  private int depid;
  private String Postdate;
  private String deadline;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDepid() {
        return depid;
    }

    public void setDepid(int depid) {
        this.depid = depid;
    }

    public String getPostdate() {
        return Postdate;
    }

    public void setPostdate(String postdate) {
        Postdate = postdate;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }
}
