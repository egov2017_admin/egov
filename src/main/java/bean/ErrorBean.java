package bean;

public class ErrorBean {
    private String erId;
    private String ersiteName;
    private String erType;
    private String erLink;
    private String erSCR;
    private String erContent;
    private String name;
    private String phone;
    private String email;
    private String erDate;
    private String erSolvedStatus;
    private String erSolvedDate;
    private String erReply;
    private String erSearchKey;

    public String getErId() {
        return erId;
    }

    public void setErId(String erId) {
        this.erId = erId;
    }

    public String getErsiteName() {
        return ersiteName;
    }

    public void setErsiteName(String ersiteName) {
        this.ersiteName = ersiteName;
    }

    public String getErType() {
        return erType;
    }

    public void setErType(String erType) {
        this.erType = erType;
    }

    public String getErLink() {
        return erLink;
    }

    public void setErLink(String erLink) {
        this.erLink = erLink;
    }

    public String getErSCR() {
        return erSCR;
    }

    public void setErSCR(String erSCR) {
        this.erSCR = erSCR;
    }

    public String getErContent() {
        return erContent;
    }

    public void setErContent(String erContent) {
        this.erContent = erContent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getErDate() {
        return erDate;
    }

    public void setErDate(String erDate) {
        this.erDate = erDate;
    }

    public String getErSolvedStatus() {
        return erSolvedStatus;
    }

    public void setErSolvedStatus(String erSolvedStatus) {
        this.erSolvedStatus = erSolvedStatus;
    }

    public String getErSolvedDate() {
        return erSolvedDate;
    }

    public void setErSolvedDate(String erSolvedDate) {
        this.erSolvedDate = erSolvedDate;
    }

    public String getErReply() {
        return erReply;
    }

    public void setErReply(String erReply) {
        this.erReply = erReply;
    }

    public String getErSearchKey() {
        return erSearchKey;
    }

    public void setErSearchKey(String erSearchKey) {
        this.erSearchKey = erSearchKey;
    }
}
