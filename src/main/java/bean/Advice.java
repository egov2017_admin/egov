package bean;

/**
 * Created by liu on 2017/9/9.
 */
public class Advice {
  private int id;
  private int replySubject;
  private String content;
  private String name;
  private String phone;
  private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReplySubject() {
        return replySubject;
    }

    public void setReplySubject(int replySubject) {
        this.replySubject = replySubject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
