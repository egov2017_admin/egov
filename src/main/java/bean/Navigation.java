package bean;

/**
 * 导航服务类
 *
 * @author dsf
 * @date|time 2017/9/8|21:45
 * @last update:
 */
public class Navigation {

    //导航条目id
    private String navId;
    //导航条目名称
    private String navName;
    //导航条目链接
    private String navLink;
    //导航条目类型（0：站点，1：部门）
    private String navType;
    //导航条目添加日期
    private String navPostDate;


    public String getNavId() {
        return navId;
    }

    public void setNavId(String navId) {
        this.navId = navId;
    }

    public String getNavName() {
        return navName;
    }

    public void setNavName(String navName) {
        this.navName = navName;
    }

    public String getNavLink() {
        return navLink;
    }

    public void setNavLink(String navLink) {
        this.navLink = navLink;
    }

    public String getNavType() {
        return navType;
    }

    public void setNavType(String navType) {
        this.navType = navType;
    }

    public String getNavPostDate() {
        return navPostDate;
    }

    public void setNavPostDate(String navPostDate) {
        this.navPostDate = navPostDate;
    }


}