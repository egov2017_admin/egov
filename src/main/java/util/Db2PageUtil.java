package util;

import java.util.Hashtable;

/**
 * 把数据库字段值转换为前台可以优雅显示的名称
 *
 * @author dsf
 * @date|time 2017/9/16|22:15
 * @update:
 */
public class Db2PageUtil {
    public static String cafType(String num) {
        // 创建Hashtable集合
        Hashtable<String, String> cafType = new Hashtable<String, String>();
        cafType.put("0", "咨询");
        cafType.put("1", "建议");
        cafType.put("2", "表扬");
        cafType.put("3", "投诉");
        cafType.put("4", "举报");
        cafType.put("5", "反弹");
        //返回key对应的value值
        return cafType.get(num);
    }

    public static String cafPublic(String num) {
        // 创建Hashtable集合
        Hashtable<String, String> cafPublic = new Hashtable<String, String>();
        cafPublic.put("0", "不公开");
        cafPublic.put("1", "公开");
        //返回key对应的value值
        return cafPublic.get(num);
    }

    public static String cafFile(String num) {
        Hashtable<String, String> cafFile = new Hashtable<String, String>();
        cafFile.put("jpg", "OK");
        cafFile.put("JPG", "OK");
        cafFile.put("png", "OK");
        cafFile.put("PNG", "OK");
        cafFile.put("doc", "OK");
        cafFile.put("DOC", "OK");
        cafFile.put("docx", "OK");
        cafFile.put("DOCX", "OK");
        cafFile.put("pdf", "OK");
        cafFile.put("PDF", "OK");
        return cafFile.get(num);
    }

    public static String cafFileName(String filename) {
        String Rfilename;
        if (StringUtil.isNotEmpty(filename)) {
            //去除唯一标识前缀，得到用户上传时的文件名称
            Rfilename = filename.substring(33, filename.length());
        } else Rfilename = "该用户并未上传附件";
        return Rfilename;
    }

    public static String cafReply(String num) {
        if (StringUtil.isEmpty(num)) {
            num = "暂无回复，请耐心等待工作人员处理。";
        }
        return num;

    }

    public static String cafSolvedStatus(String num) {
        Hashtable<String, String> cafSS = new Hashtable<String, String>();
        cafSS.put("0", "未解决");
        cafSS.put("1", "已解决");
        cafSS.put("2", "已交办");
        return cafSS.get(num);
    }
}
