package util;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by liu on 2017/9/21.
 */
public class DruidUtil {
    private static DataSource ds = null;
    static {
        Properties props = new Properties();
        try{
            // 加载配置文件
            String classpath = DruidUtil.class.getResource("/").getPath();
            //c3p0资源文件真实路径
            String websiteURL = (classpath.replace("/build/classes", "").replace("%20", " ").replace("classes/", "") + "ds.properties").replaceFirst("/", "");
            FileInputStream in = new FileInputStream(websiteURL);
            props.load(in);
            ds = DruidDataSourceFactory.createDataSource(props);
            System.out.println(ds.toString());
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
