package util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 根据id生成查询码
 *
 * @author dsf
 * @date|time 2017/9/17
 * @update:
 */
public class SearchKeyUtil {
    /**
     * 根据Id生成查询码（14-16位）（Id非法则返回ID_ERROR）
     * 【Id值域】：1-999999
     * 【格式】日期6位+2位随机码+1位类型标识(C,M,E)+（id+365）+2位随机码
     * 【格式】全大写
     * 如：合法查询码170912DSC16E8O(对应id为1)
     *
     * @param id
     * @param type
     * @return
     */
    public static String getSearchKey(String id, char type) {
        String searchKey = "";
        try {
            if (Integer.parseInt(id) >= 1 && Integer.parseInt(id) <= 999999) {
                //设置日期格式
                SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
                //获取当前时间，
                StringBuffer sb = new StringBuffer(df.format(new Date()));
                sb.append(getRandomString(2));
                sb.append(type);
                id = idDeal(id);
                sb.append(id);
                sb.append(getRandomString(2));
                searchKey = sb.toString();
            } else searchKey = "ID_ERROR";
        } catch (NumberFormatException e) {
            searchKey = "ID_ERROR";
            System.out.println("NumberFormatException：ID不合法");
        }
        return searchKey;
    }

    /**
     * 判断传入查询码是否合法
     *
     * @param sk
     * @return
     */
    public static boolean isSearchKey(String sk) {
        if (sk.length() >= 14 && sk.length() <= 16) {
            String date = sk.substring(0, 6);
            String type = sk.substring(8, 9);
            String id = sk.substring(9, sk.length() - 2);
            if (isNumeric(date) && isType(type) && isId(id)) {
                return true;
            } else return false;
        } else return false;
    }

    /**
     * 判断传入字符串是不是合法id
     *
     * @param id
     * @return
     */
    private static boolean isId(String id) {
        int a = Integer.parseInt(id, 16);
        if (a >= 366 && a <= 1000364) return true;
        else return false;
    }

    /**
     * 判断传入字符串是不是合法类型标识
     *
     * @param type
     * @return
     */
    private static boolean isType(String type) {
        if (type.equals("C") || type.equals("M") || type.equals("E")) return true;
        else return false;
    }

    /**
     * 判断字符串是不是只由数字组成
     *
     * @param str
     * @return
     */
    private static boolean isNumeric(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * 生成中间随机码
     *
     * @param length
     * @return
     */
    private static String getRandomString(int length) {
        String base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }


    /**
     * 对传入的id进行处理（加365，以及十六进制转换）
     *
     * @param id
     * @return
     */
    private static String idDeal(String id) {
        int a = 0;
        try {

            a = Integer.parseInt(id);
            a += 365;
            id = Integer.toHexString(a);

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        id = id.toUpperCase();
        return id;
    }

    public static int String2Int(String str) {
        int int_str = 0;
        try {
            int_str = Integer.valueOf(str).intValue();
            return int_str;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return int_str;
    }
}